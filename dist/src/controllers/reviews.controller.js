"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const catchAsyncErrors_1 = require("../middlewares/catchAsyncErrors");
const ErrorHandler_1 = require("../utils/ErrorHandler");
const reviews_model_1 = __importDefault(require("../models/reviews.model"));
const filesystem_1 = require("../utils/filesystem");
const path_1 = __importDefault(require("path"));
exports.default = {
    createReview: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            let filename = null;
            if (req.file) {
                filename = req.file.filename;
            }
            const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;
            const { owner_ru, owner_uz } = req.body;
            const checkOwner_ru_name = yield reviews_model_1.default.findOne({ owner_ru });
            const checkOwner_uz_name = yield reviews_model_1.default.findOne({ owner_ru });
            if (checkOwner_ru_name) {
                return next(new ErrorHandler_1.ErrorHandler(`${owner_ru} user like this name already created`, 400));
            }
            if (checkOwner_uz_name) {
                return next(new ErrorHandler_1.ErrorHandler(`${owner_uz} user like this name already created`, 400));
            }
            const newReview = yield reviews_model_1.default.create(Object.assign(Object.assign({}, req.body), { avatar: filename ? imagePath : '' }));
            res.status(201).json({
                status: 201,
                review: 'newReview',
                msg: 'ok'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })),
    deleteReview: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler('id required', 400));
            }
            const checkReviw = yield reviews_model_1.default.findById(id);
            if (!checkReviw) {
                return next(new ErrorHandler_1.ErrorHandler('review not found', 400));
            }
            yield checkReviw.deleteOne({ id });
            (0, filesystem_1.deleteImage)(path_1.default.join(process.cwd(), "public", "reviews", checkReviw.avatar.slice(32)));
            res.status(200).json({
                status: 200,
                review: checkReviw,
                msg: 'ok'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })),
    updateReview: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            let filename = null;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler('id required', 400));
            }
            const checkReviw = yield reviews_model_1.default.findById(id);
            if (!checkReviw) {
                return next(new ErrorHandler_1.ErrorHandler('review not found', 400));
            }
            if (req.file) {
                filename = req.file.filename;
            }
            const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;
            const data = filename ? Object.assign(Object.assign({}, req.body), { avatar: imagePath }) : Object.assign({}, req.body);
            const updatedReview = yield reviews_model_1.default.findByIdAndUpdate(id, { $set: data }, { new: true });
            res.status(200).json({
                status: 200,
                review: updatedReview,
                msg: "ok",
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })),
    getOneReview: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler('id required', 400));
            }
            const checkReviw = yield reviews_model_1.default.findById(id);
            if (!checkReviw) {
                return next(new ErrorHandler_1.ErrorHandler('review not found', 400));
            }
            res.status(200).json({
                status: 200,
                review: checkReviw,
                msg: "ok",
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })),
    getAllReviews: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const reviews = yield reviews_model_1.default.find();
            res.status(200).json({
                status: 200,
                reviews,
                msg: "ok",
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    }))
};
