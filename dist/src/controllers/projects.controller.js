"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const catchAsyncErrors_1 = require("../middlewares/catchAsyncErrors");
const ErrorHandler_1 = require("../utils/ErrorHandler");
const validation_1 = require("../validation/validation");
const projects_model_1 = __importDefault(require("../models/projects.model"));
const filesystem_1 = require("../utils/filesystem");
const path_1 = __importDefault(require("path"));
const PORT = process.env.PORT || 4000;
exports.default = {
    createProject: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        var _a, _b, _c;
        try {
            const { error, value } = validation_1.projectsValidation.validate(req.body);
            let filename1 = null;
            let filename2 = null;
            let filename3 = null;
            if (error) {
                return next(new ErrorHandler_1.ErrorHandler(error.message, 400));
            }
            if (req === null || req === void 0 ? void 0 : req.files) {
                filename1 = (_a = req === null || req === void 0 ? void 0 : req.files["img1"][0]) === null || _a === void 0 ? void 0 : _a.filename;
                filename2 = (_b = req === null || req === void 0 ? void 0 : req.files["img2"][0]) === null || _b === void 0 ? void 0 : _b.filename;
                filename3 = (_c = req === null || req === void 0 ? void 0 : req.files["img3"][0]) === null || _c === void 0 ? void 0 : _c.filename;
            }
            const imagePath1 = `${req.protocol}://${req.hostname}/api/media/${filename1}`;
            const imagePath2 = `${req.protocol}://${req.hostname}/api/media/${filename2}`;
            const imagePath3 = `${req.protocol}://${req.hostname}/api/media/${filename3}`;
            const images = [
                { original: imagePath1, thumbnail: imagePath1 },
                { original: imagePath2, thumbnail: imagePath2 },
                { original: imagePath3, thumbnail: imagePath3 },
            ];
            const data = Object.assign(Object.assign({}, value), { images });
            const newProject = yield projects_model_1.default.create(Object.assign({}, data));
            res.status(201).json({
                status: 201,
                project: newProject,
                msg: "ok",
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 400));
        }
    })),
    getAllProjects: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const projects = yield projects_model_1.default.find();
            res.status(201).json({
                status: 201,
                projects,
                msg: "ok",
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 400));
        }
    })),
    getOneProject: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler("id required", 400));
            }
            const project = yield projects_model_1.default.findById(id);
            if (!project) {
                return next(new ErrorHandler_1.ErrorHandler("project not found", 400));
            }
            res.status(200).json({
                status: 200,
                project,
                msg: "ok",
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 400));
        }
    })),
    deleteProject: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler("id required", 400));
            }
            const project = yield projects_model_1.default.findById(id);
            if (!project) {
                return next(new ErrorHandler_1.ErrorHandler("project not found", 400));
            }
            project.images.forEach((item) => {
                (0, filesystem_1.deleteImage)(path_1.default.join(process.cwd(), "public", "projects"
                //  item.slice(32)
                ));
            });
            yield project.deleteOne({ id });
            res.status(200).json({
                status: 200,
                project,
                msg: "ok",
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 400));
        }
    })),
    updateProject: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler("id required", 400));
            }
            const project = yield projects_model_1.default.findById(id);
            if (!project) {
                return next(new ErrorHandler_1.ErrorHandler("project not found", 400));
            }
            const data = Object.assign({}, req.body);
            const updatedProjectItem = yield projects_model_1.default.findByIdAndUpdate(id, { $set: data }, { new: true });
            res.status(200).json({
                status: 200,
                project: updatedProjectItem,
                msg: "ok",
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 400));
        }
    })),
    updateProjectImages: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        var _d;
        try {
            const { id } = req.params;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler("id required", 400));
            }
            const project = yield projects_model_1.default.findById(id);
            if (!project) {
                return next(new ErrorHandler_1.ErrorHandler("project not found", 400));
            }
            let filename1 = null;
            if (req.files) {
                for (let i in req.files) {
                    let count = 0;
                    if (i == 'img2') {
                        count = 1;
                    }
                    else if (i == 'img3') {
                        count = 2;
                    }
                    filename1 = (_d = req === null || req === void 0 ? void 0 : req.files[i][0]) === null || _d === void 0 ? void 0 : _d.filename;
                    const imagePath1 = `${req.protocol}://${req.hostname}/api/media/${filename1}`;
                    project.images[count] = { original: imagePath1, thumbnail: imagePath1 };
                    yield project.save();
                }
                res.status(200).json({
                    status: 200,
                    project: project,
                    msg: "ok",
                });
                return;
            }
            else {
                return next(new ErrorHandler_1.ErrorHandler('cannot update images', 400));
            }
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 400));
        }
    })),
};
