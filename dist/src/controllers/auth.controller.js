"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ErrorHandler_1 = require("../utils/ErrorHandler");
const user_model_1 = __importDefault(require("../models/user.model"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const catchAsyncErrors_1 = require("../middlewares/catchAsyncErrors");
const validation_1 = require("../validation/validation");
exports.default = {
    login: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        const { error, value } = validation_1.login.validate(req.body);
        if (error) {
            return next(new ErrorHandler_1.ErrorHandler('name or phone number is not valid', 400));
        }
        try {
            const { name, number } = value;
            const user = yield user_model_1.default.findOne({ name }).select('+number');
            if (!user) {
                return next(new ErrorHandler_1.ErrorHandler('Invalid name or phone number', 400));
            }
            if (user.number != number) {
                return next(new ErrorHandler_1.ErrorHandler('Invalid name or Password', 400));
            }
            const token = jsonwebtoken_1.default.sign({ id: user._id, name }, process.env.SECRET_KEY || '', {
                expiresIn: '5d'
            });
            res.status(200).json({
                status: 200,
                token,
                user,
                msg: 'user successfuly logged in'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })),
    register: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        const { error, value } = validation_1.register.validate(req.body);
        if (error) {
            return next(new ErrorHandler_1.ErrorHandler('name or phone number is not valid', 400));
        }
        try {
            const { name, number } = value;
            const user = yield user_model_1.default.findOne({ number });
            if (user) {
                return next(new ErrorHandler_1.ErrorHandler('user already registered', 400));
            }
            const newUser = yield user_model_1.default.create({
                name, number
            });
            const token = jsonwebtoken_1.default.sign({ id: newUser._id, name }, process.env.SECRET_KEY || '', {
                expiresIn: '5d'
            });
            res.status(201).json({
                status: 201,
                token,
                msg: 'user successfuly registered'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })),
};
