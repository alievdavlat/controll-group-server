"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ErrorHandler_1 = require("../utils/ErrorHandler");
const user_model_1 = __importDefault(require("../models/user.model"));
const validation_1 = require("../validation/validation");
const filesystem_1 = require("../utils/filesystem");
const path_1 = __importDefault(require("path"));
const PORT = process.env.PORT || 4000;
exports.default = {
    changeUserRole: (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        const { error, value } = validation_1.updateUserRoles.validate(req.body);
        if (error) {
            return next(new ErrorHandler_1.ErrorHandler('user_id or role is empty', 400));
        }
        try {
            const { role, user_id } = value;
            const user = yield user_model_1.default.findByIdAndUpdate(user_id, { role }, { new: true });
            res.status(200).json({
                status: 200,
                user,
                msg: 'ok'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    }),
    getProfile: (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        const { user } = req;
        const checkUser = yield user_model_1.default.findById(user._id);
        if (!checkUser) {
            return next(new ErrorHandler_1.ErrorHandler('user not found', 400));
        }
        res.status(200).json({
            status: 200,
            user: checkUser,
            msg: 'ok'
        });
    }),
    updateUserInfo: (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        var _a;
        try {
            const { name } = req.body;
            const userId = (_a = req.user) === null || _a === void 0 ? void 0 : _a._id;
            const user = yield user_model_1.default.findById(userId);
            if (name && user) {
                user.name = name;
            }
            yield (user === null || user === void 0 ? void 0 : user.save());
            res.status(200).json({
                status: 200,
                user,
                msg: 'ok'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 400));
        }
    }),
    updatePhoneNumber: (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        var _b;
        try {
            const { oldNumber, newNumber } = req.body;
            if (!oldNumber || !newNumber) {
                return next(new ErrorHandler_1.ErrorHandler("Please enter ol and new phone number", 400));
            }
            const userId = (_b = req.user) === null || _b === void 0 ? void 0 : _b._id;
            const user = yield user_model_1.default.findById(userId).select('+number');
            if (user.number === undefined) {
                return next(new ErrorHandler_1.ErrorHandler('Invalid user', 400));
            }
            if (user.number != oldNumber) {
                return next(new ErrorHandler_1.ErrorHandler('Invalid old password', 400));
            }
            user.number = newNumber;
            yield user.save();
            res.status(201).json({
                status: 201,
                success: true,
                user,
                msg: 'user password successfully updated'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 400));
        }
    }),
    updateProfilePicture: (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        var _c, _d;
        try {
            const userId = req.user._id;
            let filename = null;
            const user = yield user_model_1.default.findById(userId);
            if ((req === null || req === void 0 ? void 0 : req.file) && user) {
                if (user === null || user === void 0 ? void 0 : user.avatar) {
                    (0, filesystem_1.deleteImage)(path_1.default.join(process.cwd(), "public", user.avatar.slice(32)));
                    filename = (_c = req === null || req === void 0 ? void 0 : req.file) === null || _c === void 0 ? void 0 : _c.filename;
                    const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;
                    user.avatar = imagePath;
                }
                else {
                    filename = (_d = req === null || req === void 0 ? void 0 : req.file) === null || _d === void 0 ? void 0 : _d.filename;
                    const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;
                    user.avatar = imagePath;
                }
            }
            user.save();
            res.status(200).json({
                status: 200,
                user,
                msg: 'image uploaded'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 400));
        }
    }),
};
