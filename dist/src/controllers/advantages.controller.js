"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const catchAsyncErrors_1 = require("../middlewares/catchAsyncErrors");
const ErrorHandler_1 = require("../utils/ErrorHandler");
const validation_1 = require("../validation/validation");
const advantages_model_1 = __importDefault(require("../models/advantages.model"));
const filesystem_1 = require("../utils/filesystem");
const path_1 = __importDefault(require("path"));
const PORT = process.env.PORT || 4000;
exports.default = {
    createAdvnatages: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            let filename = null;
            const { error, value } = validation_1.createAdvnatages.validate(req.body);
            if (error) {
                return next(new ErrorHandler_1.ErrorHandler(error.message, 400));
            }
            const { title_ru, title_uz, description_ru, description_uz } = value;
            if (req.file) {
                filename = req.file.filename;
            }
            const checkAdvantagesRu = yield advantages_model_1.default.findOne({ title_ru });
            const checkAdvantagesUz = yield advantages_model_1.default.findOne({ title_uz });
            if (checkAdvantagesRu) {
                return next(new ErrorHandler_1.ErrorHandler(`${title_ru} already used`, 400));
            }
            if (checkAdvantagesUz) {
                return next(new ErrorHandler_1.ErrorHandler(`${title_uz} already used`, 400));
            }
            const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;
            const advantages = yield advantages_model_1.default.create({
                title_ru,
                title_uz,
                description_ru,
                description_uz,
                img: imagePath,
            });
            res.status(201).json({
                status: 201,
                advantages,
                msg: "ok",
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })),
    updateAdvantages: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        var _a, _b;
        try {
            const { id } = req.params;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler("id required", 400));
            }
            let filename = null;
            const advantages = yield advantages_model_1.default.findById(id);
            if (!advantages) {
                return next(new ErrorHandler_1.ErrorHandler("advantages item not found", 400));
            }
            if ((_a = req.file) === null || _a === void 0 ? void 0 : _a.filename) {
                (0, filesystem_1.deleteImage)(path_1.default.join(process.cwd(), "public", "advantages", advantages.img.slice(32)));
                filename = req.file.filename;
            }
            const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;
            const data = ((_b = req === null || req === void 0 ? void 0 : req.file) === null || _b === void 0 ? void 0 : _b.filename)
                ? Object.assign(Object.assign({}, req.body), { img: imagePath }) : Object.assign({}, req.body);
            const updateAdvantages = yield advantages_model_1.default.findByIdAndUpdate(id, { $set: data }, { new: true });
            res.status(200).json({
                status: 200,
                advantages: updateAdvantages,
                msg: "ok",
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })),
    deletAdvantags: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler("id required", 400));
            }
            const advanatges = yield advantages_model_1.default.findById(id);
            if (!advanatges) {
                return next(new ErrorHandler_1.ErrorHandler("advanatges not found", 404));
            }
            (0, filesystem_1.deleteImage)(path_1.default.join(process.cwd(), "public", "advantages", advanatges.img.slice(32)));
            yield advanatges.deleteOne({ id });
            res.status(200).json({
                status: 200,
                advanatges,
                msg: "advanatges successfully deleted",
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })),
    getOneAdvantagesById: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler("id required", 400));
            }
            const advanatges = yield advantages_model_1.default.findById(id);
            if (!advanatges) {
                return next(new ErrorHandler_1.ErrorHandler("advanatges not found", 404));
            }
            res.status(200).json({
                status: 200,
                advanatges,
                msg: "ok",
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })),
    getAllAdvantages: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const allAdvantages = yield advantages_model_1.default.find();
            res.status(200).json({
                status: 200,
                advanatges: allAdvantages,
                msg: "ok",
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })),
};
