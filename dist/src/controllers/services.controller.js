"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const catchAsyncErrors_1 = require("../middlewares/catchAsyncErrors");
const ErrorHandler_1 = require("../utils/ErrorHandler");
const validation_1 = require("../validation/validation");
const services_model_1 = __importDefault(require("../models/services.model"));
const path_1 = __importDefault(require("path"));
const filesystem_1 = require("../utils/filesystem");
const PORT = process.env.PORT || 4000;
exports.default = {
    createServices: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        var _a;
        try {
            const { error, value } = validation_1.servicesValidation.validate(req.body);
            let filename = null;
            if (error) {
                return next(new ErrorHandler_1.ErrorHandler(error.message, 400));
            }
            const { service_type_ru, service_type_uz, title_uz, title_ru, } = value;
            const checkTypeRU = yield services_model_1.default.findOne({ service_type_ru });
            const checkTypeUZ = yield services_model_1.default.findOne({ service_type_uz });
            const checkTitleRU = yield services_model_1.default.findOne({ title_ru });
            const checkTitleUZ = yield services_model_1.default.findOne({ title_uz });
            if (checkTitleRU ||
                checkTitleUZ ||
                checkTypeRU ||
                checkTypeUZ) {
                return next(new ErrorHandler_1.ErrorHandler('values  title or type already used', 400));
            }
            if (req.file) {
                filename = (_a = req === null || req === void 0 ? void 0 : req.file) === null || _a === void 0 ? void 0 : _a.filename;
            }
            const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;
            const service = yield services_model_1.default.create(Object.assign({ img: imagePath }, value));
            res.status(201).json({
                status: 201,
                service,
                msg: 'ok'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 400));
        }
    })),
    updateServiceTexts: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler('id required', 400));
            }
            const service = yield services_model_1.default.findById(id);
            if (!service) {
                return next(new ErrorHandler_1.ErrorHandler('service item not found', 404));
            }
            const data = Object.assign({}, req.body);
            const updatedService = yield services_model_1.default.findByIdAndUpdate(id, { $set: data }, { new: true });
            res.status(200).json({
                status: 200,
                updatedService,
                msg: 'ok'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 400));
        }
    })),
    updateServicesImage: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        var _b;
        try {
            const { id } = req.params;
            let filename = null;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler('id required', 400));
            }
            const service = yield services_model_1.default.findById(id);
            if (!service) {
                return next(new ErrorHandler_1.ErrorHandler('service item not found', 404));
            }
            console.log(req === null || req === void 0 ? void 0 : req.file);
            if (req === null || req === void 0 ? void 0 : req.file) {
                filename = (_b = req === null || req === void 0 ? void 0 : req.file) === null || _b === void 0 ? void 0 : _b.filename;
                const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;
                if (service === null || service === void 0 ? void 0 : service.img) {
                    (0, filesystem_1.deleteImage)(path_1.default.join(process.cwd(), "public", "services", service.img.slice(32)));
                    service.img = imagePath;
                }
                else {
                    service.img = imagePath;
                }
            }
            console.log(service);
            yield service.save();
            res.status(200).json({
                status: 200,
                service,
                msg: 'ok'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 400));
        }
    })),
    deleteService: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler('id required', 400));
            }
            const service = yield services_model_1.default.findById(id);
            if (!service) {
                return next(new ErrorHandler_1.ErrorHandler('service item not found', 404));
            }
            (0, filesystem_1.deleteImage)(path_1.default.join(process.cwd(), "public", "services", service.img.slice(32)));
            yield service.deleteOne({ id });
            res.status(200).json({
                status: 200,
                service,
                msg: 'course successfully deleted'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 400));
        }
    })),
    getOneService: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler('id required', 400));
            }
            const service = yield services_model_1.default.findById(id);
            if (!service) {
                return next(new ErrorHandler_1.ErrorHandler('service item not found', 404));
            }
            res.status(200).json({
                status: 200,
                service,
                msg: 'ok'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 400));
        }
    })),
    getAllServices: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const services = yield services_model_1.default.find();
            res.status(200).json({
                status: 200,
                services,
                msg: 'ok'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 400));
        }
    }))
};
