"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ErrorHandler_1 = require("../utils/ErrorHandler");
const stocks_model_1 = __importDefault(require("../models/stocks.model"));
const filesystem_1 = require("../utils/filesystem");
const path_1 = __importDefault(require("path"));
const PORT = process.env.PORT || 4000;
exports.default = {
    createStocks: (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        var _a;
        try {
            const { description_uz, description_ru } = req.body;
            let filename = null;
            if (req.file) {
                filename = (_a = req === null || req === void 0 ? void 0 : req.file) === null || _a === void 0 ? void 0 : _a.filename;
            }
            const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;
            const newStock = yield stocks_model_1.default.create({ description_uz, description_ru, image: imagePath });
            res.status(201).json({
                status: 201,
                stock: newStock,
                msg: 'ok'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    }),
    getAllStocks: (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const stocks = yield stocks_model_1.default.find();
            res.status(201).json({
                status: 201,
                stocks,
                msg: 'ok'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    }),
    getOneStock: (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler('id required', 400));
            }
            const stock = yield stocks_model_1.default.findById(id);
            if (!stock) {
                return next(new ErrorHandler_1.ErrorHandler('stock not found', 400));
            }
            res.status(201).json({
                status: 201,
                stock,
                msg: 'ok'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    }),
    updateStock: (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler('id required', 400));
            }
            const data = Object.assign({}, req.body);
            const stock = yield stocks_model_1.default.findByIdAndUpdate(id, { $set: data }, { new: true });
            if (!stock) {
                return next(new ErrorHandler_1.ErrorHandler('stock not found', 400));
            }
            res.status(201).json({
                status: 201,
                stock,
                msg: 'ok'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    }),
    deleteStock: (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler('id required', 400));
            }
            const stock = yield stocks_model_1.default.findById(id);
            if (!stock) {
                return next(new ErrorHandler_1.ErrorHandler('stock not found', 400));
            }
            (0, filesystem_1.deleteImage)(path_1.default.join(process.cwd(), "public", stock.image.slice(32)));
            yield stock.deleteOne({ id });
            res.status(201).json({
                status: 201,
                stock,
                msg: 'ok'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })
};
