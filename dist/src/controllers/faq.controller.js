"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ErrorHandler_1 = require("../utils/ErrorHandler");
const Faq_model_1 = __importDefault(require("../models/Faq.model"));
exports.default = {
    createFaq: (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { answer_uz, answer_ru, question_uz, question_ru } = req.body;
            if (!answer_uz || !answer_ru || !question_uz || !question_ru) {
                return next(new ErrorHandler_1.ErrorHandler('answer and question required', 400));
            }
            const faq = yield Faq_model_1.default.create({ answer_uz, answer_ru, question_uz, question_ru });
            res.status(201).json({
                status: 201,
                faq,
                msg: "ok"
            });
        }
        catch (errr) {
            return next(new ErrorHandler_1.ErrorHandler(errr.message, 500));
        }
    }),
    getAllFaq: (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const faqs = yield Faq_model_1.default.find();
            res.status(201).json({
                status: 201,
                faqs,
                msg: "ok"
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    }),
    getOneFaq: (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler('id required', 400));
            }
            const faq = yield Faq_model_1.default.findById(id);
            if (!faq) {
                return next(new ErrorHandler_1.ErrorHandler('faq item not found ', 400));
            }
            res.status(201).json({
                status: 201,
                faq,
                msg: "ok"
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    }),
    updatedFaq: (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler('id required', 400));
            }
            const data = Object.assign({}, req.body);
            console.log(req.body);
            console.log(data.length, 'datadan ');
            const faq = yield Faq_model_1.default.findByIdAndUpdate(id, { $set: data }, { new: true });
            if (!faq) {
                return next(new ErrorHandler_1.ErrorHandler('faq item not found ', 400));
            }
            res.status(201).json({
                status: 201,
                faq,
                msg: "ok"
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    }),
    deleteFaq: (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler('id required', 400));
            }
            const faq = yield Faq_model_1.default.findById(id);
            if (!faq) {
                return next(new ErrorHandler_1.ErrorHandler('faq item not found ', 400));
            }
            yield faq.deleteOne({ id });
            res.status(201).json({
                status: 201,
                faq,
                msg: "ok"
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })
};
