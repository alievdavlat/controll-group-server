"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const layout_model_1 = __importDefault(require("../models/layout.model"));
const ErrorHandler_1 = require("../utils/ErrorHandler");
const catchAsyncErrors_1 = require("../middlewares/catchAsyncErrors");
const filesystem_1 = require("../utils/filesystem");
const path_1 = __importDefault(require("path"));
const validation_1 = require("../validation/validation");
const PORT = process.env.PORT || 4000;
exports.default = {
    createLayout: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        var _a;
        try {
            const { type } = req.params;
            let filename = null;
            if (!type) {
                return next(new ErrorHandler_1.ErrorHandler("layout type required", 400));
            }
            if (req.file) {
                filename = req.file.filename;
            }
            const eachTypeExist = yield layout_model_1.default.findOne({ type });
            if (eachTypeExist) {
                return next(new ErrorHandler_1.ErrorHandler(`${type}  already exist`, 400));
            }
            if (type === "hero") {
                const { error: heroError, value: heroValue } = validation_1.createHeroValidation.validate(req.body);
                if (heroError) {
                    return next(new ErrorHandler_1.ErrorHandler("hero values required", 400));
                }
                const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;
                yield layout_model_1.default.create({
                    type: "hero",
                    hero: Object.assign({ image: imagePath }, heroValue),
                });
            }
            if (type === "about") {
                const { error, value } = validation_1.createAboutValidation.validate(req.body);
                if (error) {
                    return next(new ErrorHandler_1.ErrorHandler("about values required", 400));
                }
                const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;
                yield layout_model_1.default.create({
                    type: "about",
                    about: Object.assign({ image: imagePath }, value),
                });
            }
            if (type === "contact") {
                if ((_a = req === null || req === void 0 ? void 0 : req.file) === null || _a === void 0 ? void 0 : _a.filename) {
                    const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;
                    yield layout_model_1.default.create({
                        type: "contact",
                        contact_img: imagePath,
                    });
                }
            }
            res.status(201).json({
                status: 201,
                msg: "Layout created successfully",
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })),
    editlayout: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        var _b, _c, _d, _e, _f, _g, _h;
        try {
            const { type } = req.params;
            let filename = null;
            if (!type) {
                return next(new ErrorHandler_1.ErrorHandler("layout type required", 400));
            }
            if (req.file) {
                filename = req.file.filename;
            }
            if (type === "hero") {
                const heroData = yield layout_model_1.default.findOne({ type: "hero" });
                const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;
                if (heroData) {
                    if (req === null || req === void 0 ? void 0 : req.file) {
                        if (heroData === null || heroData === void 0 ? void 0 : heroData.hero.image) {
                            (0, filesystem_1.deleteImage)(path_1.default.join(process.cwd(), "public", "layout", (_b = heroData === null || heroData === void 0 ? void 0 : heroData.hero) === null || _b === void 0 ? void 0 : _b.image.slice(32)));
                        }
                        yield layout_model_1.default.findByIdAndUpdate(heroData._id, {
                            hero: Object.assign({ image: imagePath }, req.body),
                        });
                    }
                    else {
                        yield layout_model_1.default.findByIdAndUpdate(heroData._id, {
                            hero: Object.assign({ image: (_c = heroData === null || heroData === void 0 ? void 0 : heroData.hero) === null || _c === void 0 ? void 0 : _c.image }, req.body),
                        });
                    }
                }
                else {
                    yield layout_model_1.default.create({
                        type: "hero",
                        hero: Object.assign({ image: imagePath }, req.body),
                    });
                }
            }
            if (type === "about") {
                const aboutData = yield layout_model_1.default.findOne({ type: "about" });
                console.log(req.body);
                const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;
                if (aboutData) {
                    if (req === null || req === void 0 ? void 0 : req.file) {
                        if ((_d = aboutData === null || aboutData === void 0 ? void 0 : aboutData.about) === null || _d === void 0 ? void 0 : _d.image) {
                            (0, filesystem_1.deleteImage)(path_1.default.join(process.cwd(), "public", "layout", (_e = aboutData === null || aboutData === void 0 ? void 0 : aboutData.about) === null || _e === void 0 ? void 0 : _e.image.slice(32)));
                        }
                        yield layout_model_1.default.findByIdAndUpdate(aboutData._id, {
                            about: Object.assign({ image: imagePath }, req.body),
                        });
                    }
                    else {
                        yield layout_model_1.default.findByIdAndUpdate(aboutData._id, {
                            about: Object.assign({ image: (_f = aboutData === null || aboutData === void 0 ? void 0 : aboutData.about) === null || _f === void 0 ? void 0 : _f.image }, req.body),
                        });
                    }
                }
                else {
                    yield layout_model_1.default.create({
                        type: "about",
                        about: Object.assign({ image: imagePath }, req.body),
                    });
                }
            }
            if (type === "contact") {
                const contact = yield layout_model_1.default.findOne({ type });
                const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;
                if (contact) {
                    if (req === null || req === void 0 ? void 0 : req.file) {
                        if (contact.contact.image) {
                            (0, filesystem_1.deleteImage)(path_1.default.join(process.cwd(), "public", "layout", (_g = contact === null || contact === void 0 ? void 0 : contact.contact.image) === null || _g === void 0 ? void 0 : _g.slice(32)));
                            yield layout_model_1.default.findByIdAndUpdate(contact._id, {
                                type: "contact",
                                contact: Object.assign(Object.assign({}, req.body), { image: imagePath }),
                            });
                        }
                    }
                    else {
                        yield layout_model_1.default.findByIdAndUpdate(contact._id, {
                            type: "contact",
                            contact: Object.assign({}, req.body),
                        });
                    }
                }
                else {
                    yield layout_model_1.default.create({
                        type: "contact",
                        contact: Object.assign(Object.assign({}, req.body), { image: imagePath }),
                    });
                    console.log(req.body, "bez image");
                }
            }
            if (type === "video") {
                const videoSection = yield layout_model_1.default.findOne({ type });
                const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;
                if (videoSection && videoSection.video) {
                    (0, filesystem_1.deleteImage)(path_1.default.join(process.cwd(), "public", "layout", (_h = videoSection === null || videoSection === void 0 ? void 0 : videoSection.video) === null || _h === void 0 ? void 0 : _h.slice(32)));
                    yield layout_model_1.default.findByIdAndUpdate(videoSection._id, {
                        type: "video",
                        video: imagePath,
                    });
                }
                else {
                    yield layout_model_1.default.create({ type: "video", video: imagePath });
                }
            }
            if (type === "main") {
                const mainSection = yield layout_model_1.default.findOne({ type });
                if (mainSection) {
                    yield layout_model_1.default.findByIdAndUpdate(mainSection._id, {
                        main: Object.assign({}, req.body),
                    });
                }
                else {
                    yield layout_model_1.default.create({ type: "main", main: Object.assign({}, req.body) });
                }
            }
            if (type === "titles") {
                const titlesSection = yield layout_model_1.default.findOne({ type });
                if (titlesSection) {
                    yield layout_model_1.default.findByIdAndUpdate(titlesSection._id, {
                        titles: Object.assign({}, req.body),
                    });
                }
                else {
                    yield layout_model_1.default.create({
                        type: "titles",
                        titles: Object.assign({}, req.body),
                    });
                }
            }
            res.status(200).json({
                status: 200,
                msg: "Layout  successfully updated",
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })),
    getLayoutByType: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { type } = req.params;
            const layout = yield layout_model_1.default.findOne({ type });
            res.status(200).json({
                status: 200,
                layout,
                msg: "ok",
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })),
    deleteLayoutByType: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { type } = req.params;
            const layout = yield layout_model_1.default.findOne({ type });
            if (!layout) {
                return next(new ErrorHandler_1.ErrorHandler("layout not found", 404));
            }
            const dletedLayout = yield layout_model_1.default.findOneAndDelete({ type });
            res.status(200).json({
                status: 200,
                layout: dletedLayout,
                msg: "ok",
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })),
};
