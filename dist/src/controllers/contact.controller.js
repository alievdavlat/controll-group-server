"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const catchAsyncErrors_1 = require("../middlewares/catchAsyncErrors");
const validation_1 = require("../validation/validation");
const ErrorHandler_1 = require("../utils/ErrorHandler");
const notefication_model_1 = __importDefault(require("../models/notefication.model"));
const contact_model_1 = __importDefault(require("../models/contact.model"));
const token = '6478915425:AAHoDdO8ZPchFiL4-3L7ZpbM-Mot-Y3ie5g';
const chat_id = -4000802937;
exports.default = {
    sendContact: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { error, value } = validation_1.contactValidation.validate(req.body);
            console.log(value);
            if (error) {
                return next(new ErrorHandler_1.ErrorHandler('name or phone number must be not empty', 400));
            }
            const { name, number } = value;
            const tepm = `
          Получена новая заявка 🆕 \n
          👤 \u00A0 Имя: ${name} \n
          📞 \u00A0 Телефон: +${number}
        `;
            const encodedMessage = encodeURIComponent(tepm);
            const reques = yield fetch(`https://api.telegram.org/bot${token}/sendMessage?chat_id=${chat_id}&text=${encodedMessage}`, {
                method: 'POST',
            });
            yield notefication_model_1.default.create({
                title_ru: 'Получен новый запрос на обратную связь.',
                title_uz: 'Yangi qayta aloqa so\'rovi qabul qilindi',
                message_ru: `${name} Предоставил(а) запрос на обратную связь`,
                message_uz: `${name} Aloqa uchun so'rov berdi`,
            });
            const newContact = yield contact_model_1.default.create({ name, number });
            res.status(201).json({
                status: 201,
                newContact,
                msg: 'ok'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })),
    getAllContact: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const contacts = yield contact_model_1.default.find();
            res.status(200).json({
                status: 200,
                contacts,
                msg: 'ok'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })),
    deleteContact: (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            if (!id) {
                return next(new ErrorHandler_1.ErrorHandler('id required', 400));
            }
            const contactUser = yield contact_model_1.default.findById(id);
            if (!contactUser) {
                return next(new ErrorHandler_1.ErrorHandler('contact not found', 400));
            }
            yield contactUser.deleteOne({ id });
            res.status(200).json({
                status: 200,
                contact: contactUser,
                msg: 'ok'
            });
        }
        catch (err) {
            return next(new ErrorHandler_1.ErrorHandler(err.message, 500));
        }
    })),
};
