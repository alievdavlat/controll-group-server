"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
require("dotenv/config");
const mongoose_config_1 = __importDefault(require("./config/mongoose.config"));
const errorHandler_middleware_1 = require("./middlewares/errorHandler.middleware");
const Routes_1 = __importDefault(require("./routes/Routes"));
const path_1 = require("path");
const http_1 = __importDefault(require("http"));
const socketServer_1 = require("./socketServer");
const auth_routes_1 = __importDefault(require("./routes/auth.routes"));
const PORT = process.env.PORT || 4000;
const app = (0, express_1.default)();
const server = http_1.default.createServer(app);
app.use("/api/media", [
    express_1.default.static((0, path_1.join)(process.cwd(), "public", "gallery")),
    express_1.default.static((0, path_1.join)(process.cwd(), "public", "services")),
    express_1.default.static((0, path_1.join)(process.cwd(), "public", "contact")),
    express_1.default.static((0, path_1.join)(process.cwd(), "public", "advantages")),
    express_1.default.static((0, path_1.join)(process.cwd(), "public", "layout")),
    express_1.default.static((0, path_1.join)(process.cwd(), "public", "projects")),
    express_1.default.static((0, path_1.join)(process.cwd(), "public")),
]);
app.use(express_1.default.json({ limit: "50mb" }));
app.use((0, cors_1.default)());
app.use(Routes_1.default);
app.use(auth_routes_1.default);
app.use(errorHandler_middleware_1.errohandle);
app.get("/", (req, res, next) => {
    console.log(req.hostname, req.protocol, PORT);
    res.status(200).send("api is working");
});
// app.all('*', (req:Request, res:Response, next:NextFunction) => {
//   const err = new Error(`Route ${req.originalUrl} not found`) as any;
//   err.statusCode = 404
//   next(err)
// })
(0, socketServer_1.initSocketServer)(server);
server.listen(PORT, () => {
    console.log(`server running on port ${PORT}`);
    (0, mongoose_config_1.default)();
});
