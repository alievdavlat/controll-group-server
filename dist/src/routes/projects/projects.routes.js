"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const isAuthenticated_1 = __importDefault(require("../../middlewares/isAuthenticated"));
const isAdmin_1 = require("../../middlewares/isAdmin");
const multer_1 = require("../../utils/multer");
const projects_controller_1 = __importDefault(require("../../controllers/projects.controller"));
const projectsRouter = (0, express_1.Router)();
projectsRouter
    .post("/projects", isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)("admin"), multer_1.project_upload.fields([
    { name: "img1" },
    { name: "img2" },
    { name: "img3" },
]), projects_controller_1.default.createProject)
    .get("/projects", projects_controller_1.default.getAllProjects)
    .get("/projects/:id", projects_controller_1.default.getOneProject)
    .put("/projects/:id", isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)("admin"), projects_controller_1.default.updateProject)
    .put("/update-projects-image/:id", isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)("admin"), multer_1.project_upload.fields([
    { name: "img1" },
    { name: "img2" },
    { name: "img3" },
]), projects_controller_1.default.updateProjectImages)
    .delete("/projects/:id", isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)("admin"), projects_controller_1.default.deleteProject);
exports.default = projectsRouter;
