"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const isAuthenticated_1 = __importDefault(require("../../middlewares/isAuthenticated"));
const isAdmin_1 = require("../../middlewares/isAdmin");
const services_controller_1 = __importDefault(require("../../controllers/services.controller"));
const multer_1 = require("../../utils/multer");
const servicesRouter = (0, express_1.Router)();
servicesRouter
    .post("/create-service", isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)("admin"), multer_1.services_upload.single("img"), services_controller_1.default.createServices)
    .put("/update-services-info/:id", isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)("admin"), services_controller_1.default.updateServiceTexts)
    .put("/update-service-img/:id", isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)("admin"), multer_1.services_upload.single("image"), services_controller_1.default.updateServicesImage)
    .delete('/delete-service/:id', isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)('admin'), services_controller_1.default.deleteService)
    .get('/service/:id', services_controller_1.default.getOneService)
    .get('/services', services_controller_1.default.getAllServices);
exports.default = servicesRouter;
