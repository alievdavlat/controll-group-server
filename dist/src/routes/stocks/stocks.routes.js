"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const isAuthenticated_1 = __importDefault(require("../../middlewares/isAuthenticated"));
const isAdmin_1 = require("../../middlewares/isAdmin");
const stocks_controller_1 = __importDefault(require("../../controllers/stocks.controller"));
const multer_1 = require("../../utils/multer");
const stocksRouter = (0, express_1.Router)();
stocksRouter
    .post('/stocks', isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)('admin'), multer_1.upload.single('image'), stocks_controller_1.default.createStocks)
    .get('/stocks', stocks_controller_1.default.getAllStocks)
    .get('/stocks/:id', stocks_controller_1.default.getOneStock)
    .put('/stocks/:id', isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)('admin'), stocks_controller_1.default.updateStock)
    .delete('/stocks/:id', isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)('admin'), stocks_controller_1.default.deleteStock);
exports.default = stocksRouter;
