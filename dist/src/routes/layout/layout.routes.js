"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const multer_1 = require("../../utils/multer");
const layout_controller_1 = __importDefault(require("../../controllers/layout.controller"));
const isAdmin_1 = require("../../middlewares/isAdmin");
const isAuthenticated_1 = __importDefault(require("../../middlewares/isAuthenticated"));
const layoutRouter = (0, express_1.Router)();
layoutRouter.post("/layout/:type", isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)("admin"), multer_1.layout_upload.single("image"), layout_controller_1.default.createLayout);
layoutRouter.put("/layout/:type", isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)("admin"), multer_1.layout_upload.single("image"), layout_controller_1.default.editlayout);
layoutRouter.get('/layout/:type', layout_controller_1.default.getLayoutByType);
layoutRouter.delete('/layout/:type', isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)('admin'), layout_controller_1.default.deleteLayoutByType);
exports.default = layoutRouter;
