"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const isAuthenticated_1 = __importDefault(require("../../middlewares/isAuthenticated"));
const isAdmin_1 = require("../../middlewares/isAdmin");
const faq_controller_1 = __importDefault(require("../../controllers/faq.controller"));
const faqRouter = (0, express_1.Router)();
faqRouter
    .post('/faq', isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)('admin'), faq_controller_1.default.createFaq)
    .get('/faqs', faq_controller_1.default.getAllFaq)
    .get('/faq/:id', faq_controller_1.default.getOneFaq)
    .put('/faq/:id', isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)('admin'), faq_controller_1.default.updatedFaq)
    .delete('/faq/:id', isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)('admin'), faq_controller_1.default.deleteFaq);
exports.default = faqRouter;
