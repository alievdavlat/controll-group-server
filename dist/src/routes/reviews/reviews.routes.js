"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const reviews_controller_1 = __importDefault(require("../../controllers/reviews.controller"));
const isAuthenticated_1 = __importDefault(require("../../middlewares/isAuthenticated"));
const isAdmin_1 = require("../../middlewares/isAdmin");
const multer_1 = require("../../utils/multer");
const reviewsRouter = (0, express_1.Router)();
reviewsRouter
    .post('/create-review', multer_1.reviews_upload.single('avatar'), reviews_controller_1.default.createReview)
    .delete('/delete-review/:id', reviews_controller_1.default.deleteReview)
    .put('/update-review/:id', isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)('admin'), reviews_controller_1.default.updateReview)
    .get('/reviews', reviews_controller_1.default.getAllReviews)
    .get('/reviews/:id', reviews_controller_1.default.getOneReview);
exports.default = reviewsRouter;
