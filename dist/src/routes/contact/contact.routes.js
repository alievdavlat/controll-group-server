"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const contact_controller_1 = __importDefault(require("../../controllers/contact.controller"));
const isAuthenticated_1 = __importDefault(require("../../middlewares/isAuthenticated"));
const isAdmin_1 = require("../../middlewares/isAdmin");
const contactRouter = (0, express_1.Router)();
contactRouter
    .post('/contact', contact_controller_1.default.sendContact)
    .get('/contact', isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)('admin'), contact_controller_1.default.getAllContact)
    .delete('/contact/:id', isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)('admin'), contact_controller_1.default.deleteContact);
exports.default = contactRouter;
