"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const isAuthenticated_1 = __importDefault(require("../../middlewares/isAuthenticated"));
const isAdmin_1 = require("../../middlewares/isAdmin");
const user_controller_1 = __importDefault(require("../../controllers/user.controller"));
const multer_1 = require("../../utils/multer");
const userRouter = (0, express_1.Router)();
userRouter
    .put('/update-role', isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)('user'), user_controller_1.default.changeUserRole)
    .get('/get-profile', isAuthenticated_1.default, user_controller_1.default.getProfile)
    .put('/update-info', isAuthenticated_1.default, user_controller_1.default.updateUserInfo)
    .put('/update-number', isAuthenticated_1.default, user_controller_1.default.updatePhoneNumber)
    .put('/update-avatar', isAuthenticated_1.default, multer_1.upload.single('avatar'), user_controller_1.default.updateProfilePicture);
exports.default = userRouter;
