"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const analytics_controller_1 = __importDefault(require("../../controllers/analytics.controller"));
const isAuthenticated_1 = __importDefault(require("../../middlewares/isAuthenticated"));
const isAdmin_1 = require("../../middlewares/isAdmin");
const analyticsRouter = (0, express_1.Router)();
analyticsRouter
    .get('/analytics-contact', isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)('admin'), analytics_controller_1.default.getContactAnalytics);
exports.default = analyticsRouter;
