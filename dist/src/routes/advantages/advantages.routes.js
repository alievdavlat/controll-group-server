"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const advantages_controller_1 = __importDefault(require("../../controllers/advantages.controller"));
const isAuthenticated_1 = __importDefault(require("../../middlewares/isAuthenticated"));
const isAdmin_1 = require("../../middlewares/isAdmin");
const multer_1 = require("../../utils/multer");
const advanatgesRouter = (0, express_1.Router)();
advanatgesRouter
    .post("/create-advantages", isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)("admin"), multer_1.advantages_upload.single("img"), advantages_controller_1.default.createAdvnatages)
    .put("/update-advantages/:id", isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)("admin"), multer_1.advantages_upload.single("img"), advantages_controller_1.default.updateAdvantages)
    .get("/advantages/:id", advantages_controller_1.default.getOneAdvantagesById)
    .get("/advantages", advantages_controller_1.default.getAllAdvantages)
    .delete("/advantages/:id", isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)("admin"), advantages_controller_1.default.deletAdvantags);
exports.default = advanatgesRouter;
