"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const layout_routes_1 = __importDefault(require("./layout/layout.routes"));
const advantages_routes_1 = __importDefault(require("./advantages/advantages.routes"));
const contact_routes_1 = __importDefault(require("./contact/contact.routes"));
const users_routes_1 = __importDefault(require("./users/users.routes"));
const reviews_routes_1 = __importDefault(require("./reviews/reviews.routes"));
const services_routes_1 = __importDefault(require("./services/services.routes"));
const notefications_routes_1 = __importDefault(require("./notefications/notefications.routes"));
const analytics_routes_1 = __importDefault(require("./analytics/analytics.routes"));
const faq_routes_1 = __importDefault(require("./faq/faq.routes"));
const stocks_routes_1 = __importDefault(require("./stocks/stocks.routes"));
const projects_routes_1 = __importDefault(require("./projects/projects.routes"));
const MainRouter = (0, express_1.Router)();
exports.default = MainRouter.use([
    layout_routes_1.default,
    advantages_routes_1.default,
    contact_routes_1.default,
    users_routes_1.default,
    reviews_routes_1.default,
    services_routes_1.default,
    notefications_routes_1.default,
    analytics_routes_1.default,
    faq_routes_1.default,
    stocks_routes_1.default,
    projects_routes_1.default
]);
