"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const isAuthenticated_1 = __importDefault(require("../../middlewares/isAuthenticated"));
const isAdmin_1 = require("../../middlewares/isAdmin");
const notefication_controller_1 = __importDefault(require("../../controllers/notefication.controller"));
const noteficationsRouter = (0, express_1.Router)();
noteficationsRouter
    .get('/notefications', isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)('admin'), notefication_controller_1.default.getAllNotification)
    .put('/notefications/:id', isAuthenticated_1.default, (0, isAdmin_1.authorizeRole)('admin'), notefication_controller_1.default.updateNotefication);
exports.default = noteficationsRouter;
