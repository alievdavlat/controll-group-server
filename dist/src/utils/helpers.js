"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cleanNameFile = void 0;
const cleanNameFile = (name) => {
    try {
        return name.replace(/\s+/g, '_');
    }
    catch (error) {
        return error.message;
    }
};
exports.cleanNameFile = cleanNameFile;
