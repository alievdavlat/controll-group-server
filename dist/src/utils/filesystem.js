"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteImage = void 0;
const fs_1 = require("fs");
function deleteImage(imagePath) {
    // Use fs.unlink to delete the image file
    (0, fs_1.unlink)(imagePath, (err) => {
        if (err) {
            console.error(`Error deleting image ${imagePath}: ${err.message}`);
        }
        else {
            console.log(`Image ${imagePath} deleted successfully.`);
        }
    });
}
exports.deleteImage = deleteImage;
