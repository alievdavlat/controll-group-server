"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reviews_upload = exports.project_upload = exports.layout_upload = exports.advantages_upload = exports.services_upload = exports.gallery_upload = exports.upload = void 0;
const multer_1 = __importDefault(require("multer"));
const helpers_1 = require("../utils/helpers");
const path_1 = require("path");
const storage = multer_1.default.diskStorage({
    destination: function (req, file, cb) {
        cb(null, (0, path_1.join)(process.cwd(), "public"));
    },
    filename: function (req, file, cb) {
        const { originalname } = file;
        cb(null, (0, helpers_1.cleanNameFile)(Date.now() + originalname));
    },
});
const gallery_storage = multer_1.default.diskStorage({
    destination: function (req, file, cb) {
        cb(null, (0, path_1.join)(process.cwd(), "public", "gallery"));
    },
    filename: function (req, file, cb) {
        const { originalname } = file;
        cb(null, (0, helpers_1.cleanNameFile)(Date.now() + originalname));
    },
});
const services_storage = multer_1.default.diskStorage({
    destination: function (req, file, cb) {
        cb(null, (0, path_1.join)(process.cwd(), "public", "services"));
    },
    filename: function (req, file, cb) {
        const { originalname } = file;
        cb(null, (0, helpers_1.cleanNameFile)(Date.now() + originalname));
    },
});
const advantages_storage = multer_1.default.diskStorage({
    destination: function (req, file, cb) {
        cb(null, (0, path_1.join)(process.cwd(), "public", "advantages"));
    },
    filename: function (req, file, cb) {
        const { originalname } = file;
        cb(null, (0, helpers_1.cleanNameFile)(Date.now() + originalname));
    },
});
const layout_storage = multer_1.default.diskStorage({
    destination: function (req, file, cb) {
        cb(null, (0, path_1.join)(process.cwd(), "public", "layout"));
    },
    filename: function (req, file, cb) {
        const { originalname } = file;
        cb(null, (0, helpers_1.cleanNameFile)(Date.now() + originalname));
    },
});
const projects_storage = multer_1.default.diskStorage({
    destination: function (req, file, cb) {
        cb(null, (0, path_1.join)(process.cwd(), "public", "projects"));
    },
    filename: function (req, file, cb) {
        const { originalname } = file;
        cb(null, (0, helpers_1.cleanNameFile)(Date.now() + originalname));
    },
});
const reviews_storage = multer_1.default.diskStorage({
    destination: function (req, file, cb) {
        cb(null, (0, path_1.join)(process.cwd(), "public", "reviews"));
    },
    filename: function (req, file, cb) {
        const { originalname } = file;
        cb(null, (0, helpers_1.cleanNameFile)(Date.now() + originalname));
    },
});
exports.upload = (0, multer_1.default)({ storage });
exports.gallery_upload = (0, multer_1.default)({ storage: gallery_storage });
exports.services_upload = (0, multer_1.default)({ storage: services_storage });
exports.advantages_upload = (0, multer_1.default)({ storage: advantages_storage });
exports.layout_upload = (0, multer_1.default)({ storage: layout_storage });
exports.project_upload = (0, multer_1.default)({ storage: projects_storage });
exports.reviews_upload = (0, multer_1.default)({ storage: reviews_storage });
