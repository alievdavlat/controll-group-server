"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const numberRegexPatern = /^9989[012345789][0-9]{7}$/;
const userSchema = new mongoose_1.default.Schema({
    name: {
        type: String,
        required: [true, 'Please enter your name']
    },
    number: {
        type: String,
        required: [true, 'Please enter your phone number'],
        validate: {
            validator: function (value) {
                return numberRegexPatern.test(value);
            },
            message: 'please enter valid phone number'
        },
        unique: true,
    },
    avatar: { type: String },
    role: {
        type: String,
        default: 'user',
    },
}, {
    timestamps: true
});
const userModel = mongoose_1.default.model("Users", userSchema);
exports.default = userModel;
