"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const faqSchema = new mongoose_1.default.Schema({
    answer_uz: { type: String },
    answer_ru: { type: String },
    question_uz: { type: String },
    question_ru: { type: String }
}, {
    timestamps: true
});
const faqModel = mongoose_1.default.model("faq", faqSchema);
exports.default = faqModel;
