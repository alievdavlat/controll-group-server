"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const servicesSchema = new mongoose_1.default.Schema({
    service_type_ru: {
        type: String,
        required: [true, 'Please enter services type']
    },
    service_type_uz: {
        type: String,
        required: [true, 'Please enter services type']
    },
    title_ru: {
        type: String,
        required: [true, 'Please enterservices title']
    },
    title_uz: {
        type: String,
        required: [true, 'Please enterservices title']
    },
    subtitle_one_ru: {
        type: String,
        required: [true, 'Please enter services subtitle']
    },
    subtitle_one_uz: {
        type: String,
        required: [true, 'Please enter services subtitle']
    },
    subtitle_two_ru: {
        type: String,
        required: [true, 'Please enter services subtitle']
    },
    subtitle_two_uz: {
        type: String,
        required: [true, 'Please enter services subtitle']
    },
    description_uz: {
        type: String,
        required: [true, 'Please enter services description'],
    },
    description_ru: {
        type: String,
        required: [true, 'Please enter services description'],
    },
    img: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});
const servicesModel = mongoose_1.default.model("services", servicesSchema);
exports.default = servicesModel;
