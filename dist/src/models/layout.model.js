"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const layoutSchema = new mongoose_1.Schema({
    type: { type: String },
    about: {
        title_ru: { type: String },
        title_uz: { type: String },
        subtitle_ru: { type: String },
        subtitle_uz: { type: String },
        image: { type: String },
        counts: [
            {
                count: { type: String },
                title_ru: { type: String },
                title_uz: { type: String },
            },
        ],
    },
    hero: {
        image: { type: String },
        title_ru: { type: String },
        title_uz: { type: String },
        subtitle_ru: { type: String },
        subtitle_uz: { type: String },
        hero_services_uz: {
            type: [String],
            required: true,
        },
        hero_services_ru: {
            type: [String],
            required: true,
        },
    },
    contact: {
        title_ru: { type: String },
        description_ru: { type: String },
        title_uz: { type: String },
        description_uz: { type: String },
        image: { type: String },
    },
    titles: {
        hero: {
            title_ru: { type: String },
            title_uz: { type: String },
            label_1_uz: { type: String },
            label_1_ru: { type: String },
            label_2_uz: { type: String },
            label_2_ru: { type: String },
        },
        about: {
            title_ru: { type: String },
            title_uz: { type: String },
            label_1_uz: { type: String },
            label_1_ru: { type: String },
            label_2_uz: { type: String },
            label_2_ru: { type: String },
        },
        services: {
            title_ru: { type: String },
            title_uz: { type: String },
            label_1_uz: { type: String },
            label_1_ru: { type: String },
            label_2_uz: { type: String },
            label_2_ru: { type: String },
        },
        advantage: {
            title_ru: { type: String },
            title_uz: { type: String },
            label_1_uz: { type: String },
            label_1_ru: { type: String },
            label_2_uz: { type: String },
            label_2_ru: { type: String },
        },
        projects: {
            title_ru: { type: String },
            title_uz: { type: String },
            label_1_uz: { type: String },
            label_1_ru: { type: String },
            label_2_uz: { type: String },
            label_2_ru: { type: String },
        },
        stocks: {
            title_ru: { type: String },
            title_uz: { type: String },
            label_1_uz: { type: String },
            label_1_ru: { type: String },
            label_2_uz: { type: String },
            label_2_ru: { type: String },
        },
        faqs: {
            title_ru: { type: String },
            title_uz: { type: String },
            label_1_uz: { type: String },
            label_1_ru: { type: String },
            label_2_uz: { type: String },
            label_2_ru: { type: String },
        },
        reviws: {
            title_ru: { type: String },
            title_uz: { type: String },
            label_1_uz: { type: String },
            label_1_ru: { type: String },
            label_2_uz: { type: String },
            label_2_ru: { type: String },
        },
    },
    main: {
        primary_phone: { type: String },
        secondary_phone: { type: String },
        address_ru: { type: String },
        address_uz: { type: String },
        socials: [
            {
                icon: { type: String },
                path: { type: String },
                name: { type: String },
            },
        ],
    },
    video: { type: String },
});
const LayoutModel = (0, mongoose_1.model)('Layout', layoutSchema);
exports.default = LayoutModel;
