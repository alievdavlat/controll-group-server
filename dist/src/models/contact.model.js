"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const contactSchema = new mongoose_1.default.Schema({
    name: {
        type: String,
        required: [true, 'Please enter your name']
    },
    number: {
        type: String,
        required: [true, 'Please enter your phone number'],
        unique: true
    }
}, {
    timestamps: true
});
const contactModel = mongoose_1.default.model("contacts", contactSchema);
exports.default = contactModel;
