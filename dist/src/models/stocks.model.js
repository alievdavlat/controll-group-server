"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const stockSchema = new mongoose_1.default.Schema({
    description_uz: {
        type: String
    },
    description_ru: {
        type: String
    },
    image: {
        type: String
    }
}, {
    timestamps: true
});
const stockModel = mongoose_1.default.model("stocks", stockSchema);
exports.default = stockModel;
