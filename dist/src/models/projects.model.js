"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const projectsSchema = new mongoose_1.default.Schema({
    address: {
        type: String,
        required: [true, 'Please enter address'],
    },
    measurment: {
        type: String,
        required: [true, 'Please enter measurment'],
    },
    deadline_ru: {
        type: String,
    },
    deadline_uz: {
        type: String,
    },
    images: [
        {
            original: {
                type: String,
                required: true,
            },
            thumbnail: {
                type: String,
                required: true,
            },
        },
    ],
}, {
    timestamps: true,
});
const projectsModel = mongoose_1.default.model("projects", projectsSchema);
exports.default = projectsModel;
