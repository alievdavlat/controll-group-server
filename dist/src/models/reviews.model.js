"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const reviewsSchema = new mongoose_1.default.Schema({
    owner_ru: {
        type: String,
        required: [true, 'Please enter owner name']
    },
    owner_uz: {
        type: String,
        required: [true, 'Please enter owner name']
    },
    description_uz: {
        type: String,
        required: [true, 'Please enter review description'],
    },
    description_ru: {
        type: String,
        required: [true, 'Please enter review description'],
    },
    avatar: {
        type: String,
    }
}, {
    timestamps: true
});
const reviewsModel = mongoose_1.default.model("reviews", reviewsSchema);
exports.default = reviewsModel;
