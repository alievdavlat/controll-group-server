"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const advantagesSchema = new mongoose_1.default.Schema({
    title_ru: {
        type: String,
        required: [true, 'Please enter advanatges title']
    },
    title_uz: {
        type: String,
        required: [true, 'Please enter advanatges title']
    },
    description_uz: {
        type: String,
        required: [true, 'Please enter advantages description'],
    },
    description_ru: {
        type: String,
        required: [true, 'Please enter advantages description'],
    },
    img: {
        type: String,
        required: true,
    }
}, {
    timestamps: true
});
const advanatgesModel = mongoose_1.default.model("advantages", advantagesSchema);
exports.default = advanatgesModel;
