"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.catchAsynError = void 0;
const catchAsynError = (theFunc) => (req, res, next) => {
    Promise.resolve(theFunc(req, res, next)).catch(next);
};
exports.catchAsynError = catchAsynError;
