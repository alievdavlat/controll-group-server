"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.isAuthenticated = void 0;
const ErrorHandler_1 = require("../utils/ErrorHandler");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const catchAsyncErrors_1 = require("./catchAsyncErrors");
const user_model_1 = __importDefault(require("../models/user.model"));
exports.isAuthenticated = (0, catchAsyncErrors_1.catchAsynError)((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const token = req.headers.token;
    if (!token) {
        return next(new ErrorHandler_1.ErrorHandler('Please login to access to resource', 401));
    }
    const decoded = jsonwebtoken_1.default.verify(token, process.env.SECRET_KEY);
    if (!decoded) {
        return next(new ErrorHandler_1.ErrorHandler(' access token is not valid', 403));
    }
    const user = yield user_model_1.default.findById(decoded.id);
    if (!user) {
        return next(new ErrorHandler_1.ErrorHandler('Please login to access to resource', 404));
    }
    req.user = user;
    next();
}));
exports.default = exports.isAuthenticated;
