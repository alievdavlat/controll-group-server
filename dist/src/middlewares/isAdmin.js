"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authorizeRole = void 0;
const ErrorHandler_1 = require("../utils/ErrorHandler");
//validate user roles 
const authorizeRole = (...roles) => {
    return (req, res, next) => {
        if (!roles.includes(req.user.role || '')) {
            return next(new ErrorHandler_1.ErrorHandler(`Role ${req.user.role} is not allowed to access this resource`, 403));
        }
        next();
    };
};
exports.authorizeRole = authorizeRole;
