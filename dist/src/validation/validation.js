"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.projectsValidation = exports.servicesValidation = exports.reviewValidation = exports.contactValidation = exports.createAdvnatages = exports.updateUserRoles = exports.createHeroValidation = exports.createAboutValidation = exports.register = exports.login = void 0;
const joi_1 = __importDefault(require("joi"));
const login = joi_1.default.object().keys({
    name: joi_1.default.string().required(),
    number: joi_1.default.string().required()
});
exports.login = login;
const register = joi_1.default.object().keys({
    name: joi_1.default.string().required(),
    number: joi_1.default.string().required(),
});
exports.register = register;
const createHeroValidation = joi_1.default.object().keys({
    title_uz: joi_1.default.string().required().max(100),
    title_ru: joi_1.default.string().required().max(100),
    subtitle_ru: joi_1.default.string().required().max(70),
    subtitle_uz: joi_1.default.string().required().max(70),
    hero_services: joi_1.default.array()
});
exports.createHeroValidation = createHeroValidation;
const createAboutValidation = joi_1.default.object().keys({
    title_uz: joi_1.default.string().required().max(100),
    title_ru: joi_1.default.string().required().max(100),
    subtitle_ru: joi_1.default.string().required().max(70),
    subtitle_uz: joi_1.default.string().required().max(70),
    counts: joi_1.default.array()
});
exports.createAboutValidation = createAboutValidation;
const updateUserRoles = joi_1.default.object().keys({
    user_id: joi_1.default.string().required(),
    role: joi_1.default.string().required()
});
exports.updateUserRoles = updateUserRoles;
const createAdvnatages = joi_1.default.object().keys({
    title_uz: joi_1.default.string().required(),
    title_ru: joi_1.default.string().required(),
    description_ru: joi_1.default.string().required(),
    description_uz: joi_1.default.string().required(),
});
exports.createAdvnatages = createAdvnatages;
const contactValidation = joi_1.default.object().keys({
    name: joi_1.default.string().required(),
    number: joi_1.default.string().required()
});
exports.contactValidation = contactValidation;
const reviewValidation = joi_1.default.object().keys({
    owner_ru: joi_1.default.string().required(),
    owner_uz: joi_1.default.string().required(),
    description_ru: joi_1.default.string().required(),
    description_uz: joi_1.default.string().required(),
});
exports.reviewValidation = reviewValidation;
const servicesValidation = joi_1.default.object().keys({
    service_type_ru: joi_1.default.string().required(),
    service_type_uz: joi_1.default.string().required(),
    title_uz: joi_1.default.string().required(),
    title_ru: joi_1.default.string().required(),
    subtitle_one_ru: joi_1.default.string().required(),
    subtitle_one_uz: joi_1.default.string().required(),
    subtitle_two_uz: joi_1.default.string().required(),
    subtitle_two_ru: joi_1.default.string().required(),
    description_ru: joi_1.default.string().required(),
    description_uz: joi_1.default.string().required(),
});
exports.servicesValidation = servicesValidation;
const projectsValidation = joi_1.default.object().keys({
    address: joi_1.default.string().required(),
    measurment: joi_1.default.string().required(),
    deadline_uz: joi_1.default.string().required(),
    deadline_ru: joi_1.default.string().required()
});
exports.projectsValidation = projectsValidation;
