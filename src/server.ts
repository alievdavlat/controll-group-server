import express, { Application, NextFunction, Request, Response } from "express";
import cors from "cors";
import "dotenv/config";
import connectDB from "./config/mongoose.config";
import { errohandle } from "./middlewares/errorHandler.middleware";
import MainRouter from "./routes/Routes";
import path, { join } from "path";
import http from "http";
import { initSocketServer } from "./socketServer";
import authRouter from "./routes/auth.routes";
import mongo from "./config/mongoose.config";
import { IUser } from "./models/user.model";
import LayoutModel from "./models/layout.model";

const PORT: string | number = process.env.PORT || 4000;

const app: Application = express();

const server = http.createServer(app);



declare global {
  namespace Express {
    interface Request {
      user?:IUser
    }
  }
}





app.use("/api/media", [
  express.static(join(process.cwd(), "public", "gallery")),
  express.static(join(process.cwd(), "public", "services")),
  express.static(join(process.cwd(), "public", "contact")),
  express.static(join(process.cwd(), "public", "advantages")),
  express.static(join(process.cwd(), "public", "layout")),
  express.static(join(process.cwd(), "public", "projects")),
  express.static(join(process.cwd(), "public")),
]);

app.use(express.json({ limit: "50mb" }));

app.use(cors());


app.use(MainRouter);
app.use(authRouter);
app.use(errohandle);

app.get("/", (req: Request, res: Response, next: NextFunction) => {
  console.log(req.hostname, req.protocol, PORT);

  res.status(200).send("api is working");
});

// app.all('*', (req:Request, res:Response, next:NextFunction) => {
//   const err = new Error(`Route ${req.originalUrl} not found`) as any;
//   err.statusCode = 404
//   next(err)
// })

initSocketServer(server);

server.listen(PORT, () => {
  console.log(`server running on port ${PORT}`);
  connectDB();
});
