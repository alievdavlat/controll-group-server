import mongoose , { Document, Model, Schema} from "mongoose";


export interface IStcok extends Document{
  description_uz:string
  description_ru:string
  image:string
}


const stockSchema: Schema<IStcok> = new mongoose.Schema({
  
  description_uz :{
    type :String
  },
  
  description_ru :{
    type :String
  },
  image:{
    type:String
  }

}, {
  timestamps: true
})







const stockModel:Model<IStcok> = mongoose.model("stocks", stockSchema)


export default  stockModel