import mongoose , { Document, Model, Schema} from "mongoose";


export interface IAdvantages extends Document{
  title_ru: string;
  title_uz: string;
  img: string;
  description_ru: string;
  description_uz:string
}


const advantagesSchema: Schema<IAdvantages> = new mongoose.Schema({
  title_ru:{
    type:String,
    required:[true, 'Please enter advanatges title']
  },

  title_uz:{
    type:String,
    required:[true, 'Please enter advanatges title']
  },

  description_uz:{
    type:String,
    required:[true, 'Please enter advantages description'],
  },
  description_ru:{
    type:String,
    required:[true, 'Please enter advantages description'],
  },

  img:{
    type:String,
    required:true,
  }

}, {
  timestamps: true
})




const advanatgesModel:Model<IAdvantages> = mongoose.model("advantages", advantagesSchema)


export default  advanatgesModel