import mongoose, { Document, Model,Schema } from "mongoose";


export interface INotefication  extends Document {
  title_uz: string;
  title_ru: string;
  message_ru: string;
  message_uz: string;
  status: string;
  userId: string;
}

const noteficationSchema = new Schema<INotefication>({
  title_ru:{
    type:String,
    required:true,
  },

  title_uz:{
    type:String,
    required:true,
  },

  message_uz:{
    type:String,
    required:true,
  },

  message_ru:{
    type:String,
    required:true,
  },
  status:{
    type:String,
    required:true,
    default:"unread"
  },

}, {
  timestamps:true
})


const NoteficationModel:Model<INotefication> = mongoose.model('Notefication', noteficationSchema);

export default NoteficationModel