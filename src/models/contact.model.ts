import mongoose , { Document, Model, Schema} from "mongoose";


export interface Icontact extends Document{
  name:string;
  number:string;

}


const contactSchema: Schema<Icontact> = new mongoose.Schema({
  name:{
    type:String,
    required:[true, 'Please enter your name']
  },

  number:{
    type:String,
    required:[true, 'Please enter your phone number'],
    unique:true
  }

}, {
  timestamps: true
})







const contactModel:Model<Icontact> = mongoose.model("contacts", contactSchema)


export default  contactModel