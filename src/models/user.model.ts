import mongoose , { Document, Model, Schema} from "mongoose";
import bcrypt from 'bcrypt'
const numberRegexPatern: RegExp = /^9989[012345789][0-9]{7}$/;


export interface IUser extends Document{
  name:string;

  number:string;

  avatar?: string;

  role: string

}


const userSchema: Schema<IUser> = new mongoose.Schema({
  name:{
    type:String,
    required:[true, 'Please enter your name']
  },

  number:{
    type:String,
    required:[true, 'Please enter your phone number'],
    validate:{
      validator: function ( value : string){
        return numberRegexPatern.test(value)
      },
      message:'please enter valid phone number'
    },
    unique:true,
   
  },

  avatar: {type : String},

  role:{
    type:String,
    default:'user',
  },


}, {
  timestamps: true
})




const userModel:Model<IUser> = mongoose.model("Users", userSchema)


export default  userModel