import { string } from "joi";
import mongoose, { Document,Schema, model } from "mongoose";
import path from "path";



interface aboutCountItem {
  count: number;
  title_ru: string;
  title_uz: string;
}


interface aboutItem extends  Document {
  title_ru: string
  title_uz: string,
  subtitle_ru:string,
  subtitle_uz:string,
  image:string,
  counts?: aboutCountItem[]
}



interface heroServicesItem  {
  hero_services_uz:string;
  hero_services_ru:string

}


interface IHero {
    image:string,
    title_ru:string,
    title_uz:string,
    subtitle_ru:string,
    subtitle_uz:string,
    hero_services:heroServicesItem[],
}

interface IContactSchema {
  title_ru: string;
  description_ru: string;
  title_uz: string;
  description_uz: string;
  image: string;
}

interface ITitles {
  hero: {
    title_ru: string;
    title_uz: string;
    label_1_uz: string;
    label_1_ru: string;
    label_2_uz: string;
    label_2_ru: string;

  };
  about: {
    title_ru: string;
    title_uz: string;
    label_1_uz: string;
    label_1_ru: string;
    label_2_uz: string;
    label_2_ru: string;
  };
  services: {
    title_ru: string;
    title_uz: string;
    label_1_uz: string;
    label_1_ru: string;
    label_2_uz: string;
    label_2_ru: string;
  };
  advantage: {
    title_ru: string;
    title_uz: string;
    label_1_uz: string;
    label_1_ru: string;
    label_2_uz: string;
    label_2_ru: string;
  };
  projects: {
    title_ru: string;
    title_uz: string;
    label_1_uz: string;
    label_1_ru: string;
    label_2_uz: string;
    label_2_ru: string;
  };
  stocks: {
    title_ru: string;
    title_uz: string;
    label_1_uz: string;
    label_1_ru: string;
    label_2_uz: string;
    label_2_ru: string;
  };
  faqs: {
    title_ru: string;
    title_uz: string;
    label_1_uz: string;
    label_1_ru: string;
    label_2_uz: string;
    label_2_ru: string;
  };
  reviws: {
    title_ru: string;
    title_uz: string;
    label_1_uz: string;
    label_1_ru: string;
    label_2_uz: string;
    label_2_ru: string;
  };
}

interface ISocial {
  icon:string;
  path:string;
  name:string
}
interface Imain {
  primary_phone:string;
  secondary_phone:string;
  address_uz:string;
  address_ru:string;
  titles:ITitles;
  socials:ISocial[];
}

interface Layout extends Document {
  type: string;
  about: aboutItem;
  hero: IHero;
  contact: IContactSchema;
  video: string;
  main:Imain;
  titles:ITitles;
}



const layoutSchema = new Schema({
  type: { type: String },

  about: {
    title_ru: { type: String },
    title_uz: { type: String },
    subtitle_ru: { type: String },
    subtitle_uz: { type: String },
    image: { type: String },
    counts: [
      {
        count: { type: String },
        title_ru: { type: String },
        title_uz: { type: String },
      },
    ],
  },

  hero: {
    image: { type: String },
    title_ru: { type: String },
    title_uz: { type: String },
    subtitle_ru: { type: String },
    subtitle_uz: { type: String },
    hero_services_uz: {
      type: [String],
      required: true,
    },
    hero_services_ru: {
      type: [String],
      required: true,
    },
  },

  contact: {
    title_ru: { type: String },
    description_ru: { type: String },
    title_uz: { type: String },
    description_uz: { type: String },
    image: { type: String },
  },

  titles: {
    hero: {
      title_ru: { type: String },
      title_uz: { type: String },
      label_1_uz: { type: String },
      label_1_ru: { type: String },
      label_2_uz: { type: String },
      label_2_ru: { type: String },
     },
    about: {
      title_ru: { type: String },
      title_uz: { type: String },
      label_1_uz: { type: String },
      label_1_ru: { type: String },
      label_2_uz: { type: String },
      label_2_ru: { type: String },

    },
    services: {
      title_ru: { type: String },
      title_uz: { type: String },
      label_1_uz: { type: String },
      label_1_ru: { type: String },
      label_2_uz: { type: String },
      label_2_ru: { type: String },

    },
    advantage: {
      title_ru: { type: String },
      title_uz: { type: String },
      label_1_uz: { type: String },
      label_1_ru: { type: String },
      label_2_uz: { type: String },
      label_2_ru: { type: String },

    },
    projects: {
      title_ru: { type: String },
      title_uz: { type: String },
      label_1_uz: { type: String },
      label_1_ru: { type: String },
      label_2_uz: { type: String },
      label_2_ru: { type: String },

    },
    stocks: {
      title_ru: { type: String },
      title_uz: { type: String },
      label_1_uz: { type: String },
      label_1_ru: { type: String },
      label_2_uz: { type: String },
      label_2_ru: { type: String },

    },
    faqs: {
      title_ru: { type: String },
      title_uz: { type: String },
      label_1_uz: { type: String },
      label_1_ru: { type: String },
      label_2_uz: { type: String },
      label_2_ru: { type: String },

    },
    reviws: {
      title_ru: { type: String },
      title_uz: { type: String },
      label_1_uz: { type: String },
      label_1_ru: { type: String },
      label_2_uz: { type: String },
      label_2_ru: { type: String },

    },
  },

  main: {
    primary_phone: { type: String },
    secondary_phone: { type: String },
    address_ru: { type: String },
    address_uz: { type: String },

    socials: [
      {
        icon: { type: String },
        path: { type: String },
        name: { type: String },
      },
    ],
  },

  video: { type: String },
});


const LayoutModel = model<Layout>('Layout', layoutSchema);

export default LayoutModel;

