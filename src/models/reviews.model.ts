


import mongoose , { Document, Model, Schema} from "mongoose";


export interface IReview extends Document{
  owner_ru: string;
  owner_uz:string;
  description_ru: string;
  description_uz:string;
  avatar:string;
}


const reviewsSchema: Schema<IReview> = new mongoose.Schema({
  owner_ru:{
    type:String,
    required:[true, 'Please enter owner name']
  },

  owner_uz:{
    type:String,
    required:[true, 'Please enter owner name']
  },

  description_uz:{
    type:String,
    required:[true, 'Please enter review description'],
  },
  description_ru:{
    type:String,
    required:[true, 'Please enter review description'],
  },
  avatar:{
    type:String,
  }

}, {
  timestamps: true
})




const reviewsModel:Model<IReview> = mongoose.model("reviews", reviewsSchema)


export default  reviewsModel