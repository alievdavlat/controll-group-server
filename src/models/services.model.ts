


import mongoose , { Document, Model, Schema} from "mongoose";


export interface IServices extends Document{
  service_type_ru: string;
  service_type_uz:string;
  title_ru:string;
  title_uz:string;
  description_ru: string;
  description_uz:string,
  subtitle_one_ru:string;
  subtitle_one_uz:string;
  subtitle_two_uz:string;
  subtitle_two_ru:string;
  img: string;
}


const servicesSchema: Schema<IServices> = new mongoose.Schema({
 
  service_type_ru:{
    type:String,
    required:[true, 'Please enter services type']
  },

  service_type_uz:{
    type:String,
    required:[true, 'Please enter services type']
  },

  title_ru:{
    type:String,
    required:[true, 'Please enterservices title']
  },

  title_uz:{
    type:String,
    required:[true, 'Please enterservices title']
  },

  subtitle_one_ru:{
    type:String,
    required:[true, 'Please enter services subtitle']
  },
  subtitle_one_uz:{
    type:String,
    required:[true, 'Please enter services subtitle']
  },
  subtitle_two_ru:{
    type:String,
    required:[true, 'Please enter services subtitle']
  },
  subtitle_two_uz:{
    type:String,
    required:[true, 'Please enter services subtitle']
  },

  description_uz:{
    type:String,
    required:[true, 'Please enter services description'],
  },
  description_ru:{
    type:String,
    required:[true, 'Please enter services description'],
  },
  
  img:{
    type :String,
    required:true
  }


}, {
  timestamps: true
})




const servicesModel:Model<IServices> = mongoose.model("services", servicesSchema)


export default  servicesModel