import mongoose, { Document, Model, Schema } from "mongoose";

export interface IProjectData extends Document {
  address: string;
  measurment: string;
  deadline_ru: string;
  deadline_uz: string;
  images: { original: string; thumbnail: string }[];
}

const projectsSchema: Schema<IProjectData> = new mongoose.Schema(
  {
    address: {
      type: String,
      required: [true, 'Please enter address'],
    },

    measurment: {
      type: String,
      required: [true, 'Please enter measurment'],
    },
    deadline_ru: {
      type: String,
    },
    deadline_uz: {
      type: String,
    },
    images: [
      {
        original: {
          type: String,
          required: true,
        },
        thumbnail: {
          type: String,
          required: true,
        },
      },
    ],
  },
  {
    timestamps: true,
  }
);

const projectsModel: Model<IProjectData> = mongoose.model("projects", projectsSchema);

export default projectsModel;
