

import mongoose , { Document, Model, Schema} from "mongoose";


export interface IFaq extends Document{
answer_uz:string,
question_ru:string,
answer_ru:string,
question_uz:string
}


const faqSchema: Schema<IFaq> = new mongoose.Schema({
  
  answer_uz:{type:String},
  answer_ru:{type:String},
  question_uz:{type:String},
  question_ru:{type:String}

}, {
  timestamps: true
})




const faqModel:Model<IFaq> = mongoose.model("faq", faqSchema)


export default  faqModel