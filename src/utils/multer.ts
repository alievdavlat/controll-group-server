import multer from "multer";
import { cleanNameFile } from "../utils/helpers";
import { join } from "path";

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null,join(process.cwd(), "public"));
  },

  filename: function (req, file, cb) {
    const { originalname } = file;
    cb(null,  cleanNameFile(Date.now() + originalname));
  },
});

const gallery_storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, join(process.cwd(), "public", "gallery"));
  },

  filename: function (req, file, cb) {
    const { originalname } = file;
    cb(null,  cleanNameFile(Date.now() + originalname));
  },
});


const services_storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, join(process.cwd(), "public", "services"));
  },

  filename: function (req, file, cb) {
    const { originalname } = file;
    cb(null,  cleanNameFile(Date.now() + originalname));
  },
});



const advantages_storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, join(process.cwd(), "public", "advantages"));
  },

  filename: function (req, file, cb) {
    const { originalname } = file;
    cb(null,  cleanNameFile(Date.now() + originalname));
  },
});


const layout_storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, join(process.cwd(), "public", "layout"));
  },

  filename: function (req, file, cb) {
    const { originalname } = file;
    cb(null, cleanNameFile(Date.now() + originalname));
  },
});

const projects_storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, join(process.cwd(), "public", "projects"));
  },

  filename: function (req, file, cb) {
    const { originalname } = file;
    cb(null, cleanNameFile(Date.now() + originalname));
  },
});

const reviews_storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, join(process.cwd(), "public", "reviews"));
  },

  filename: function (req, file, cb) {
    const { originalname } = file;
    cb(null, cleanNameFile(Date.now() + originalname));
  },
});

export const upload = multer({ storage });
export const gallery_upload = multer({ storage: gallery_storage });
export const services_upload = multer({ storage: services_storage });
export const advantages_upload = multer({ storage: advantages_storage });
export const layout_upload = multer({ storage: layout_storage });
export const project_upload = multer({ storage: projects_storage });
export const reviews_upload = multer({ storage: reviews_storage });
