import { rmSync, unlink} from 'fs';



export function deleteImage(imagePath) {
  // Use fs.unlink to delete the image file
  unlink(imagePath, (err) => {
    if (err) {
      console.error(`Error deleting image ${imagePath}: ${err.message}`);
    } else {
      console.log(`Image ${imagePath} deleted successfully.`);
    }
  });
}





