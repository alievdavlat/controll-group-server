

export const cleanNameFile = (name:string) => {
  try {
    return name.replace(/\s+/g, '_');
  } catch (error) {
    return error.message;
  }
};
