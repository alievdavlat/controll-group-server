import joi from 'joi'

const login = joi.object().keys({
  name:joi.string().required(),
  number:joi.string().required()
})


const register = joi.object().keys({
  name:joi.string().required(),
  number:joi.string().required(),
})



const createHeroValidation = joi.object().keys({
  title_uz:joi.string().required().max(100),
  title_ru:joi.string().required().max(100),
  subtitle_ru:joi.string().required().max(70),
  subtitle_uz:joi.string().required().max(70),
  hero_services:joi.array()
})



const createAboutValidation = joi.object().keys({
  title_uz:joi.string().required().max(100),
  title_ru:joi.string().required().max(100),
  subtitle_ru:joi.string().required().max(70),
  subtitle_uz:joi.string().required().max(70),
  counts:joi.array()
})


const updateUserRoles = joi.object().keys({
  user_id:joi.string().required(),
  role:joi.string().required()
})

const createAdvnatages = joi.object().keys({
  title_uz:joi.string().required(),
  title_ru:joi.string().required(),
  description_ru:joi.string().required(),
  description_uz:joi.string().required(),
})


const contactValidation = joi.object().keys({
  name:joi.string().required(),
  number:joi.string().required()
})


const reviewValidation = joi.object().keys({
  owner_ru:joi.string().required(),
  owner_uz:joi.string().required(),
  description_ru:joi.string().required(),
  description_uz:joi.string().required(),
})

const servicesValidation = joi.object().keys({
  service_type_ru:joi.string().required(),
  service_type_uz:joi.string().required(),
  title_uz:joi.string().required(),
  title_ru:joi.string().required(),
  subtitle_one_ru:joi.string().required(),
  subtitle_one_uz:joi.string().required(),
  subtitle_two_uz:joi.string().required(),
  subtitle_two_ru:joi.string().required(),
  description_ru:joi.string().required(),
  description_uz:joi.string().required(),
})

const projectsValidation =  joi.object().keys({
  address:joi.string().required(),
  measurment:joi.string().required(),
  deadline_uz:joi.string().required(),
  deadline_ru:joi.string().required()
})

export {
  login, 
  register, 
  createAboutValidation, 
  createHeroValidation, 
  updateUserRoles,
  createAdvnatages,
  contactValidation,
  reviewValidation,
  servicesValidation,
  projectsValidation
}

