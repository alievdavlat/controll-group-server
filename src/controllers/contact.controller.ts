
import { NextFunction, Request, Response } from 'express'
import {catchAsynError} from '../middlewares/catchAsyncErrors'
import { contactValidation } from '../validation/validation'
import { ErrorHandler } from '../utils/ErrorHandler'
import NoteficationModel from '../models/notefication.model'
import contactModel from '../models/contact.model'

const token = '6478915425:AAHoDdO8ZPchFiL4-3L7ZpbM-Mot-Y3ie5g';
const chat_id = -4000802937


export default {

  sendContact:catchAsynError(async (req:Request, res:Response, next:NextFunction) => {
    try {
      const { error , value } = contactValidation.validate(req.body)

      console.log(value);
      

      if (error) {
        return next(new ErrorHandler('name or phone number must be not empty', 400))
      }

      const {name, number } = value

      const tepm = `
          Получена новая заявка 🆕 \n
          👤 \u00A0 Имя: ${name} \n
          📞 \u00A0 Телефон: +${number}
        `;

      const encodedMessage = encodeURIComponent(tepm);

      const reques = await fetch(`https://api.telegram.org/bot${token}/sendMessage?chat_id=${chat_id}&text=${encodedMessage}`, {
        method: 'POST',
      });


      await NoteficationModel.create({
        title_ru:'Получен новый запрос на обратную связь.',
        title_uz:'Yangi qayta aloqa so\'rovi qabul qilindi',
        message_ru:`${name} Предоставил(а) запрос на обратную связь`,
        message_uz:`${name} Aloqa uchun so'rov berdi`,
       })

      const newContact = await contactModel.create({name, number})

      res.status(201).json({
        status:201,
        newContact,
        msg:'ok'
      })
    } catch (err) {
      return next(new ErrorHandler(err.message, 500))
    }
  }),

  getAllContact: catchAsynError(async (req:Request, res:Response, next:NextFunction) => {

    try {
      const contacts =  await contactModel.find()

      res.status(200).json({
        status:200,
        contacts,
        msg:'ok'
      })

    } catch (err) {
      return next(new ErrorHandler(err.message, 500))      
    }

  }),

  deleteContact:catchAsynError(async (req:Request, res:Response, next:NextFunction) => {
    try {
      
      const { id } = req.params

      if (!id) {
        return next(new ErrorHandler('id required', 400))
      }


      const contactUser = await contactModel.findById(id)


      if (!contactUser) {
        return next(new ErrorHandler('contact not found', 400))
      }

      await contactUser.deleteOne({id})

      res.status(200).json({
        status:200,
        contact:contactUser,
        msg:'ok'
      })
      
    } catch (err) {
      return next(new ErrorHandler(err.message, 500))      
    }
  }),




}