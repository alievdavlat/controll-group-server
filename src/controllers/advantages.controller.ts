import { NextFunction, Request, Response } from "express";
import { catchAsynError } from "../middlewares/catchAsyncErrors";
import { ErrorHandler } from "../utils/ErrorHandler";
import { createAdvnatages } from '../validation/validation'
import advanatgesModel from "../models/advantages.model";
import { deleteImage } from "../utils/filesystem";
import path from "path";

const PORT: string | number = process.env.PORT || 4000;

export default {
  createAdvnatages: catchAsynError(
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        let filename = null;

        const { error, value } = createAdvnatages.validate(req.body);

        if (error) {
          return next(new ErrorHandler(error.message, 400));
        }

        const { title_ru, title_uz, description_ru, description_uz } = value;

        if (req.file) {
          filename = req.file.filename;
        }

        const checkAdvantagesRu = await advanatgesModel.findOne({ title_ru });
        const checkAdvantagesUz = await advanatgesModel.findOne({ title_uz });

        if (checkAdvantagesRu) {
          return next(new ErrorHandler(`${title_ru} already used`, 400));
        }

        if (checkAdvantagesUz) {
          return next(new ErrorHandler(`${title_uz} already used`, 400));
        }

        const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;

        const advantages = await advanatgesModel.create({
          title_ru,
          title_uz,
          description_ru,
          description_uz,
          img: imagePath,
        });

        res.status(201).json({
          status: 201,
          advantages,
          msg: "ok",
        });
      } catch (err) {
        return next(new ErrorHandler(err.message, 500));
      }
    }
  ),

  updateAdvantages: catchAsynError(
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { id } = req.params;

        if (!id) {
          return next(new ErrorHandler("id required", 400));
        }

        let filename = null;

        const advantages = await advanatgesModel.findById(id);

        if (!advantages) {
          return next(new ErrorHandler("advantages item not found", 400));
        }

        if (req.file?.filename) {
          deleteImage(
            path.join(
              process.cwd(),
              "public",
              "advantages",
              advantages.img.slice(32)
            )
          );
          filename = req.file.filename;
        }

        const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;

        const data = req?.file?.filename
          ? { ...req.body, img: imagePath }
          : { ...req.body };

        const updateAdvantages = await advanatgesModel.findByIdAndUpdate(
          id,
          { $set: data },
          { new: true }
        );

        res.status(200).json({
          status: 200,
          advantages: updateAdvantages,
          msg: "ok",
        });
      } catch (err) {
        return next(new ErrorHandler(err.message, 500));
      }
    }
  ),

  deletAdvantags: catchAsynError(
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { id } = req.params;

        if (!id) {
          return next(new ErrorHandler("id required", 400));
        }

        const advanatges = await advanatgesModel.findById(id);

        if (!advanatges) {
          return next(new ErrorHandler("advanatges not found", 404));
        }

        deleteImage(
          path.join(
            process.cwd(),
            "public",
            "advantages",
            advanatges.img.slice(32)
          )
        );

        await advanatges.deleteOne({ id });

        res.status(200).json({
          status: 200,
          advanatges,
          msg: "advanatges successfully deleted",
        });
      } catch (err) {
        return next(new ErrorHandler(err.message, 500));
      }
    }
  ),

  getOneAdvantagesById: catchAsynError(
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { id } = req.params;

        if (!id) {
          return next(new ErrorHandler("id required", 400));
        }

        const advanatges = await advanatgesModel.findById(id);

        if (!advanatges) {
          return next(new ErrorHandler("advanatges not found", 404));
        }

        res.status(200).json({
          status: 200,
          advanatges,
          msg: "ok",
        });
      } catch (err) {
        return next(new ErrorHandler(err.message, 500));
      }
    }
  ),

  getAllAdvantages: catchAsynError(
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const allAdvantages = await advanatgesModel.find();

        res.status(200).json({
          status: 200,
          advanatges: allAdvantages,
          msg: "ok",
        });
      } catch (err) {
        return next(new ErrorHandler(err.message, 500));
      }
    }
  ),
};
