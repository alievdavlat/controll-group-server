import { NextFunction, Request, Response } from "express";
import { ErrorHandler } from "../utils/ErrorHandler";
import stockModel from "../models/stocks.model";
import { deleteImage } from "../utils/filesystem";
import path from "path";

const PORT: string | number = process.env.PORT || 4000;

export default {
  createStocks:async (req:Request, res:Response, next:NextFunction) => {
    try {
      const { description_uz, description_ru } = req.body
      let filename = null
      if (req.file) {
        filename  = req?.file?.filename
      }

      const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;

      const newStock = await stockModel.create({description_uz, description_ru, image:imagePath})

      res.status(201).json({
        status:201,
        stock:newStock,
        msg:'ok'
      })
    } catch (err) {
      return next(new ErrorHandler(err.message , 500))
    }
  },

  getAllStocks: async (req:Request, res:Response, next:NextFunction) => {
    try {
      const stocks = await stockModel.find()
      res.status(201).json({
        status:201,
        stocks,
        msg:'ok'
      })
    } catch (err) {
      return next(new ErrorHandler(err.message , 500))
    }
  },

  getOneStock: async (req:Request, res:Response, next:NextFunction) => {
    try {
      const { id } = req.params

      if (!id) {
        return next(new ErrorHandler('id required', 400))
      }

      const stock = await stockModel.findById(id)

      if (!stock) {
        return next(new ErrorHandler('stock not found', 400))
      }


      res.status(201).json({
        status:201,
        stock,
        msg:'ok'
      })
    } catch (err) {
      return next(new ErrorHandler(err.message , 500))
    }
  },

  updateStock: async (req:Request, res:Response, next:NextFunction) => {
    try {
      const { id } = req.params

      if (!id) {
        return next(new ErrorHandler('id required', 400))
      }

      const data = {...req.body}

     

      const stock = await stockModel.findByIdAndUpdate( id,
        { $set: data},
        { new: true })

      if (!stock) {
        return next(new ErrorHandler('stock not found', 400))
      }

      

      res.status(201).json({
        status:201,
        stock,
        msg:'ok'
      })
    } catch (err) {
      return next(new ErrorHandler(err.message , 500))
    }
  },

  deleteStock: async (req:Request, res:Response, next:NextFunction) => {
    try {
      const { id } = req.params

      if (!id) {
        return next(new ErrorHandler('id required', 400))
      }

      const stock = await stockModel.findById(id)

      if (!stock) {
        return next(new ErrorHandler('stock not found', 400))
      }
      
      deleteImage(
        path.join(
          process.cwd(),
          "public",
          stock.image.slice(32)
        )
      );
      await stock.deleteOne({id})

      res.status(201).json({
        status:201,
        stock,
        msg:'ok'
      })
    } catch (err) {
      return next(new ErrorHandler(err.message , 500))
    }
  }
}