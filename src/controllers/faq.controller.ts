import { NextFunction, Request, Response } from "express";
import { ErrorHandler } from "../utils/ErrorHandler";
import faqModel from "../models/Faq.model";


export default {

  createFaq:async(req:Request, res:Response, next:NextFunction)  => {
    try { 
        const {answer_uz,answer_ru  ,question_uz,  question_ru} = req.body

        if (!answer_uz || !answer_ru || !question_uz ||  !question_ru) {
            return next(new ErrorHandler('answer and question required', 400))
        }

        const faq = await faqModel.create({answer_uz,answer_ru  ,question_uz,  question_ru})

        res.status(201).json({
          status:201,
          faq,
          msg:"ok"
        })
    } catch (errr) {
      return next(new ErrorHandler(errr.message, 500))
    }
  },
  getAllFaq:async(req:Request, res:Response, next:NextFunction)  => {
    try {
      
      const faqs = await faqModel.find()

      res.status(201).json({
        status:201,
        faqs,
        msg:"ok"
      })

    } catch (err) {
      return next(new ErrorHandler(err.message, 500))
    }
  },
  getOneFaq:async(req:Request, res:Response, next:NextFunction)  => {
    try {
      
      const { id } = req.params
      if (!id) {
          return next(new ErrorHandler('id required', 400))
      }

      const faq = await faqModel.findById(id)

      if (!faq) {
        return next(new ErrorHandler('faq item not found ' , 400))
      }
      
      res.status(201).json({
        status:201,
        faq,
        msg:"ok"
      })

    } catch (err) {
      return next(new ErrorHandler(err.message, 500))
    }
  },
  updatedFaq:async(req:Request, res:Response, next:NextFunction)  => {
    try {
      
      const { id } = req.params
      if (!id) {
          return next(new ErrorHandler('id required', 400))
      }

      const data = {...req.body}

      console.log(req.body);
      
      console.log(data.length, 'datadan ');
      

     

      const faq = await faqModel.findByIdAndUpdate( id,
        { $set: data},
        { new: true })

        if (!faq) {
          return next(new ErrorHandler('faq item not found ' , 400))
        }

      res.status(201).json({
        status:201,
        faq,
        msg:"ok"
      })

    } catch (err) {
      return next(new ErrorHandler(err.message, 500))
    }
  },
  deleteFaq:async(req:Request, res:Response, next:NextFunction)  => {
    try {
      
      const { id } = req.params
      if (!id) {
          return next(new ErrorHandler('id required', 400))
      }


      const faq = await faqModel.findById(id)

      if (!faq) {
        return next(new ErrorHandler('faq item not found ' , 400))
      }
      
      await faq.deleteOne({id})

      res.status(201).json({
        status:201,
        faq,
        msg:"ok"
      })

    } catch (err) {
      return next(new ErrorHandler(err.message, 500))
    }
  }


}