import { NextFunction, Request, Response } from "express";
import { catchAsynError } from "../middlewares/catchAsyncErrors";
import { ErrorHandler } from "../utils/ErrorHandler";
import { projectsValidation } from "../validation/validation";
import projectsModel from "../models/projects.model";
import { deleteImage } from "../utils/filesystem";
import path from "path";
const PORT: string | number = process.env.PORT || 4000;

export default {
  createProject: catchAsynError(
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { error, value } = projectsValidation.validate(req.body);

        let filename1 = null;
        let filename2 = null;
        let filename3 = null;

        if (error) {
          return next(new ErrorHandler(error.message, 400));
        }

        if (req?.files) {
          filename1 = req?.files["img1"][0]?.filename;
          filename2 = req?.files["img2"][0]?.filename;
          filename3 = req?.files["img3"][0]?.filename;
        }

        const imagePath1 = `${req.protocol}://${req.hostname}/api/media/${filename1}`;
        const imagePath2 = `${req.protocol}://${req.hostname}/api/media/${filename2}`;
        const imagePath3 = `${req.protocol}://${req.hostname}/api/media/${filename3}`;

        const images = [
          { original: imagePath1, thumbnail: imagePath1 },
          { original: imagePath2, thumbnail: imagePath2 },
          { original: imagePath3, thumbnail: imagePath3 },
        ];

        const data = { ...value, images };

        const newProject = await projectsModel.create({ ...data });

        res.status(201).json({
          status: 201,
          project: newProject,
          msg: "ok",
        });
      } catch (err) {
        return next(new ErrorHandler(err.message, 400));
      }
    }
  ),

  getAllProjects: catchAsynError(
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const projects = await projectsModel.find();
        res.status(201).json({
          status: 201,
          projects,
          msg: "ok",
        });
      } catch (err) {
        return next(new ErrorHandler(err.message, 400));
      }
    }
  ),
  getOneProject: catchAsynError(
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { id } = req.params;
        if (!id) {
          return next(new ErrorHandler("id required", 400));
        }

        const project = await projectsModel.findById(id);

        if (!project) {
          return next(new ErrorHandler("project not found", 400));
        }

        res.status(200).json({
          status: 200,
          project,
          msg: "ok",
        });
      } catch (err) {
        return next(new ErrorHandler(err.message, 400));
      }
    }
  ),
  deleteProject: catchAsynError(
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { id } = req.params;
        if (!id) {
          return next(new ErrorHandler("id required", 400));
        }

        const project = await projectsModel.findById(id);

        if (!project) {
          return next(new ErrorHandler("project not found", 400));
        }

        project.images.forEach((item) => {
          deleteImage(
            path.join(
              process.cwd(),
              "public",
              "projects"
              //  item.slice(32)
            )
          );
        });

        await project.deleteOne({ id });
        res.status(200).json({
          status: 200,
          project,
          msg: "ok",
        });
      } catch (err) {
        return next(new ErrorHandler(err.message, 400));
      }
    }
  ),
  updateProject: catchAsynError(
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { id } = req.params;

        if (!id) {
          return next(new ErrorHandler("id required", 400));
        }

        const project = await projectsModel.findById(id);

        if (!project) {
          return next(new ErrorHandler("project not found", 400));
        }
        const data = { ...req.body };

        const updatedProjectItem = await projectsModel.findByIdAndUpdate(
          id,
          { $set: data },
          { new: true }
        );

        res.status(200).json({
          status: 200,
          project: updatedProjectItem,
          msg: "ok",
        });
      } catch (err) {
        return next(new ErrorHandler(err.message, 400));
      }
    }
  ),
  updateProjectImages: catchAsynError(
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { id } = req.params;

        if (!id) {
          return next(new ErrorHandler("id required", 400));
        }

        const project = await projectsModel.findById(id);        

        if (!project) {
          return next(new ErrorHandler("project not found", 400));
        }

        let filename1 = null;

        if (req.files) {
          
        for (let i in req.files) {
          let count = 0
          
          if (i == 'img2') {
            count = 1
          } else if (i == 'img3') {
            count = 2
          }
          filename1 = req?.files[i][0]?.filename;
          const imagePath1 = `${req.protocol}://${req.hostname}/api/media/${filename1}`;
          project.images[count] = { original: imagePath1, thumbnail: imagePath1 }
          
          await project.save();
        }

        res.status(200).json({
          status: 200,
          project: project,
          msg: "ok",
        });
        return  

      } else {
        return next(new ErrorHandler('cannot update images', 400))
      }
       


      } catch (err) {
        return next(new ErrorHandler(err.message, 400));
      }
    }
  ),
};
