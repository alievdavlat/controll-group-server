import { NextFunction, Request, Response } from "express";
import { ErrorHandler } from "../utils/ErrorHandler";
import userModel from "../models/user.model";
import jwt from 'jsonwebtoken'
import { catchAsynError } from "../middlewares/catchAsyncErrors";
import { register, login } from "../validation/validation";


export default {

login: catchAsynError(async (req:Request, res:Response, next:NextFunction) => {
  const {error, value } = login.validate(req.body)


  if (error) {
    return next(new ErrorHandler('name or phone number is not valid', 400))
  }

  try {
    const { name , number } = value

      
    const user = await userModel.findOne({name}).select('+number');

    
     
  if (!user) {
    return next(new ErrorHandler('Invalid name or phone number', 400))
  }




  if (user.number != number) {
    return next(new ErrorHandler('Invalid name or Password', 400))
  }


  const token = jwt.sign({id:user._id, name}, process.env.SECRET_KEY || '', {
    expiresIn: '5d'
  })


  res.status(200).json({
    status: 200,
    token,
    user,
    msg:'user successfuly logged in'
  })

  } catch (err) {
    return next(new ErrorHandler(err.message, 500));
  }

}),

register: catchAsynError(async (req:Request, res:Response, next:NextFunction) => {
  
  const {error, value } = register.validate(req.body)

  
  if (error) {
    return next(new ErrorHandler('name or phone number is not valid', 400))
  }

  try {
    const { name , number } = value

    
    const user = await userModel.findOne({number});

    
  if (user) { 
    return next(new ErrorHandler('user already registered', 400))
  }

  const newUser = await userModel.create({
    name, number
  });

  const token = jwt.sign({id:newUser._id, name}, process.env.SECRET_KEY || '', {
    expiresIn: '5d'
  })

  res.status(201).json({
    status:201,
    token,
    msg:'user successfuly registered'
  })

  } catch (err) {
    return next(new ErrorHandler(err.message, 500));
  }

}),


}