import { Request, Response, NextFunction } from "express"
import { ErrorHandler } from "../utils/ErrorHandler"
import { generateLast12MonthData } from "../utils/analyticsGenerator"
import { catchAsynError } from "../middlewares/catchAsyncErrors"
import contactModel from "../models/contact.model"

export default {


getContactAnalytics: catchAsynError(async (req:Request, res:Response, next : NextFunction) => {
  try {
    
    const contactAnalytics  = await generateLast12MonthData(contactModel)

    res.status(200).json({
      status:200,
      analytics:contactAnalytics,
      msg:'ok'
    })

  } catch (err) {
    return next(new ErrorHandler(err.message, 500))
  }
}),








}