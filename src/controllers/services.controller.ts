import { NextFunction, Request, Response } from "express";
import { catchAsynError } from "../middlewares/catchAsyncErrors";
import { ErrorHandler } from "../utils/ErrorHandler";
import { servicesValidation } from "../validation/validation";
import servicesModel from "../models/services.model";
import path from "path";
import { deleteImage } from "../utils/filesystem";

const PORT: string | number = process.env.PORT || 4000;


export default {
  createServices: catchAsynError(
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { error, value } = servicesValidation.validate(req.body);

        let filename = null

        if (error) {
          return next(new ErrorHandler(error.message, 400));
        }

        const {
          service_type_ru,
          service_type_uz,
          title_uz,
          title_ru,
        } = value

        
        const checkTypeRU = await servicesModel.findOne({service_type_ru})
        const checkTypeUZ = await servicesModel.findOne({service_type_uz})
        const checkTitleRU = await servicesModel.findOne({title_ru})
        const checkTitleUZ = await servicesModel.findOne({title_uz})


        if (
          checkTitleRU ||
          checkTitleUZ ||
          checkTypeRU || 
          checkTypeUZ          
        ) {
          return next(new ErrorHandler('values  title or type already used', 400))
        }

        if (req.file) {
          filename  = req?.file?.filename
        }

        const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;

        const service = await servicesModel.create({ img:imagePath, ...value})

        res.status(201).json({
          status:201,
          service, 
          msg:'ok'
        })



      } catch (err) {
        return next(new ErrorHandler(err.message, 400));
      }
    }
  ),

  updateServiceTexts: catchAsynError(
    async (req: Request, res: Response, next: NextFunction) => { 

      try {
        
        const { id } = req.params


        if (!id) {
          return next(new ErrorHandler('id required', 400))
        }

        const service = await servicesModel.findById(id)

        if (!service) {
          return next(new ErrorHandler('service item not found', 404))
        }

        const data = {...req.body}
        const updatedService = await servicesModel.findByIdAndUpdate(id, {$set:data}, {new:true})

        res.status(200).json({
          status:200,
          updatedService,
          msg:'ok'
        });
      } catch (err) {
        return next(new ErrorHandler(err.message, 400));
      }

  }),

  updateServicesImage: catchAsynError(
    async (req: Request, res: Response, next: NextFunction) => { 

      try {
        const { id } = req.params
        let filename = null
        

        if (!id) {
          return next(new ErrorHandler('id required', 400))
        }

        const service = await servicesModel.findById(id)

        if (!service) {
          return next(new ErrorHandler('service item not found', 404))
        }

        
        console.log(req?.file);
        
        if (req?.file) {

          filename = req?.file?.filename
          const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;

          if (service?.img) {
            deleteImage(
              path.join(
                process.cwd(),
                "public",
                "services",
                service.img.slice(32)
              )
            );

            service.img = imagePath
          } else {
            service.img = imagePath
          }
          
        }

        console.log(service);
        
        await service.save()
        
        res.status(200).json({
          status:200,
          service,
          msg:'ok'
        })

      } catch (err) {
        return next(new ErrorHandler(err.message, 400));
      }
  }),

  deleteService :  catchAsynError(
      async (req: Request, res: Response, next: NextFunction) => { 
        try {
          const { id } = req.params


          if (!id) {
            return next(new ErrorHandler('id required', 400))
          }
  
          const service = await servicesModel.findById(id)
  
          if (!service) {
            return next(new ErrorHandler('service item not found', 404))
          }

          deleteImage(
            path.join(
              process.cwd(),
              "public",
              "services",
              service.img.slice(32)
            )
          );

          await service.deleteOne({id})
          res.status(200).json({
            status:200,
            service,
            msg:'course successfully deleted'
          })
        } catch (err) {
          return next(new ErrorHandler(err.message, 400));
        }
  }),

  getOneService: catchAsynError(
      async (req: Request, res: Response, next: NextFunction) => { 
        try {
          const { id } = req.params


          if (!id) {
            return next(new ErrorHandler('id required', 400))
          }
  
          const service = await servicesModel.findById(id)
  
          if (!service) {
            return next(new ErrorHandler('service item not found', 404))
          }

          res.status(200).json({
            status:200,
            service,
            msg:'ok'
          })


        } catch (err) {
          return next(new ErrorHandler(err.message, 400));
        }
  }),

  getAllServices: catchAsynError(
    async (req: Request, res: Response, next: NextFunction) => { 
      try {
        
        const services = await servicesModel.find()



        res.status(200).json({
          status:200,
          services,
          msg:'ok'
        })

      } catch (err) {
        return next(new ErrorHandler(err.message, 400));
      }
  })
};
