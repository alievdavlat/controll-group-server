import { NextFunction, Request, Response } from "express";
import LayoutModel from "../models/layout.model";
import { ErrorHandler } from "../utils/ErrorHandler";
import { catchAsynError } from "../middlewares/catchAsyncErrors";
import { deleteImage } from "../utils/filesystem";
import path from "path";
import {
  createHeroValidation,
  createAboutValidation,
} from "../validation/validation";

const PORT: string | number = process.env.PORT || 4000;

export default {
  createLayout: catchAsynError(
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { type } = req.params;
        let filename = null;

        if (!type) {
          return next(new ErrorHandler("layout type required", 400));
        }

        if (req.file) {
          filename = req.file.filename;
        }

        const eachTypeExist = await LayoutModel.findOne({ type });

        if (eachTypeExist) {
          return next(new ErrorHandler(`${type}  already exist`, 400));
        }

        if (type === "hero") {
          const { error: heroError, value: heroValue } =
            createHeroValidation.validate(req.body);

          if (heroError) {
            return next(new ErrorHandler("hero values required", 400));
          }

          const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;

          await LayoutModel.create({
            type: "hero",
            hero: { image: imagePath, ...heroValue },
          });
        }

        if (type === "about") {
          const { error, value } = createAboutValidation.validate(req.body);

          if (error) {
            return next(new ErrorHandler("about values required", 400));
          }

          const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;

          await LayoutModel.create({
            type: "about",
            about: { image: imagePath, ...value },
          });
        }

        if (type === "contact") {
          if (req?.file?.filename) {
            const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;

            await LayoutModel.create({
              type: "contact",
              contact_img: imagePath,
            });
          }
        }

        res.status(201).json({
          status: 201,
          msg: "Layout created successfully",
        });
      } catch (err) {
        return next(new ErrorHandler(err.message, 500));
      }
    }
  ),

  editlayout: catchAsynError(
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { type } = req.params;
        let filename = null;

        if (!type) {
          return next(new ErrorHandler("layout type required", 400));
        }

        if (req.file) {
          filename = req.file.filename;
        }

        if (type === "hero") {
          const heroData = await LayoutModel.findOne({ type: "hero" });
          const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;

          if (heroData) {
            if (req?.file) {
              if (heroData?.hero.image) {
                deleteImage(
                  path.join(
                    process.cwd(),
                    "public",
                    "layout",
                    heroData?.hero?.image.slice(32)
                  )
                );
              }

              await LayoutModel.findByIdAndUpdate(heroData._id, {
                hero: { image: imagePath, ...req.body },
              });
            } else {
              await LayoutModel.findByIdAndUpdate(heroData._id, {
                hero: { image: heroData?.hero?.image, ...req.body },
              });
            }
          } else {
            await LayoutModel.create({
              type: "hero",
              hero: { image: imagePath, ...req.body },
            });
          }
        }

        if (type === "about") {
          const aboutData = await LayoutModel.findOne({ type: "about" });
          console.log(req.body);

          const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;

          if (aboutData) {
            if (req?.file) {
              if (aboutData?.about?.image) {
                deleteImage(
                  path.join(
                    process.cwd(),
                    "public",
                    "layout",
                    aboutData?.about?.image.slice(32)
                  )
                );
              }

              await LayoutModel.findByIdAndUpdate(aboutData._id, {
                about: { image: imagePath, ...req.body },
              });
            } else {
              await LayoutModel.findByIdAndUpdate(aboutData._id, {
                about: { image: aboutData?.about?.image, ...req.body },
              });
            }
          } else {
            await LayoutModel.create({
              type: "about",
              about: { image: imagePath, ...req.body },
            });
          }
        }

        if (type === "contact") {
          const contact = await LayoutModel.findOne({ type });
          const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;

          if (contact) {
            
            if (req?.file) {

              if (contact.contact.image) {
                deleteImage(
                  path.join(
                    process.cwd(),
                    "public",
                    "layout",
                    contact?.contact.image?.slice(32)
                  )
                );

                await LayoutModel.findByIdAndUpdate(contact._id, {
                  type: "contact",
                  contact: { ...req.body, image: imagePath },
                });
              }
            } else {
             
              await LayoutModel.findByIdAndUpdate(contact._id, {
                type: "contact",
                contact: { ...req.body},
              });
            }

          } else {
            await LayoutModel.create({
              type: "contact",
              contact: { ...req.body, image: imagePath },
            });
            console.log(req.body, "bez image");
          }
        }

        if (type === "video") {
          const videoSection = await LayoutModel.findOne({ type });
          const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;

          if (videoSection && videoSection.video) {
            deleteImage(
              path.join(
                process.cwd(),
                "public",
                "layout",
                videoSection?.video?.slice(32)
              )
            );
            await LayoutModel.findByIdAndUpdate(videoSection._id, {
              type: "video",
              video: imagePath,
            });
          } else {
            await LayoutModel.create({ type: "video", video: imagePath });
          }
        }

        if (type === "main") {
          const mainSection = await LayoutModel.findOne({ type });

          if (mainSection) {
            await LayoutModel.findByIdAndUpdate(mainSection._id, {
              main: { ...req.body },
            });
          } else {
            await LayoutModel.create({ type: "main", main: { ...req.body } });
          }
        }

        if (type === "titles") {
          const titlesSection = await LayoutModel.findOne({ type });

          if (titlesSection) {
            await LayoutModel.findByIdAndUpdate(titlesSection._id, {
              titles: { ...req.body },
            });
          } else {
            await LayoutModel.create({
              type: "titles",
              titles: { ...req.body },
            });
          }
        }

        res.status(200).json({
          status: 200,
          msg: "Layout  successfully updated",
        });
      } catch (err) {
        return next(new ErrorHandler(err.message, 500));
      }
    }
  ),

  getLayoutByType: catchAsynError(
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { type } = req.params;
        const layout = await LayoutModel.findOne({ type });

        res.status(200).json({
          status: 200,
          layout,
          msg: "ok",
        });
      } catch (err) {
        return next(new ErrorHandler(err.message, 500));
      }
    }
  ),
  deleteLayoutByType: catchAsynError(
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { type } = req.params;

        const layout = await LayoutModel.findOne({ type });

        if (!layout) {
          return next(new ErrorHandler("layout not found", 404));
        }

        const dletedLayout = await LayoutModel.findOneAndDelete({ type });

        res.status(200).json({
          status: 200,
          layout: dletedLayout,
          msg: "ok",
        });
      } catch (err) {
        return next(new ErrorHandler(err.message, 500));
      }
    }
  ),
};
