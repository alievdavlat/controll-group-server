import { NextFunction, Request, Response } from "express";
import { ErrorHandler } from "../utils/ErrorHandler";
import userModel from "../models/user.model";
import { updateUserRoles } from "../validation/validation";
import { deleteImage } from "../utils/filesystem";
import path from "path";
import { IUpdatePhoneNumber, IUpdateUserInfo } from "../types";
const PORT: string | number = process.env.PORT || 4000;


export default  {
  changeUserRole: async (req:Request, res:Response, next:NextFunction) => {
    const { error , value } = updateUserRoles.validate(req.body)
      

    if (error) {
      return next(new ErrorHandler('user_id or role is empty', 400))
    }



    try {
      const { role, user_id } = value
      const user = await userModel.findByIdAndUpdate(user_id, { role}, { new:true})
      
      res.status(200).json({
        status:200,
        user,
        msg:'ok'
      })

      
    } catch (err) {
      return next(new ErrorHandler(err.message, 500))
    }

  },
  
  getProfile: async (req:Request, res:Response, next:NextFunction) => {
    const { user } = req
    
    const checkUser = await userModel.findById(user._id)
    
    if (!checkUser) {
    return next(new ErrorHandler('user not found', 400))
    }

    res.status(200).json({
      status:200,
      user:checkUser,
      msg:'ok'
    })
  },
  
  updateUserInfo : async (req:Request, res:Response, next :NextFunction) => {
    try {
      const { name } = req.body as IUpdateUserInfo


   
      

      const userId = req.user?._id

      const user = await userModel.findById(userId)

     

      if (name && user) {
          user.name = name
      }

      await user?.save()


      res.status(200).json({
        status:200,
        user,
        msg:'ok'
      })

    } catch (err) {
      return next(new ErrorHandler(err.message, 400))
    }
},
  updatePhoneNumber: async (req:Request, res:Response, next :NextFunction) => {
    try {
      
      const { oldNumber, newNumber} = req.body as IUpdatePhoneNumber

      if (!oldNumber  || !newNumber) {
        return next(new ErrorHandler("Please enter ol and new phone number", 400))
      }

      const userId = req.user?._id
      const user = await userModel.findById(userId).select('+number')      


      if (user.number === undefined) {
        return next(new ErrorHandler('Invalid user', 400))
      }

      

      if (user.number != oldNumber) {
        return next(new ErrorHandler('Invalid old password', 400))
      }

      user.number = newNumber;

      await user.save()


      res.status(201).json({
        status:201,
        success:true,
        user,
        msg:'user password successfully updated'
      })

    } catch (err) {
      return next(new ErrorHandler(err.message, 400))
    }
  },
  updateProfilePicture: async (req:Request, res:Response, next:NextFunction) => {
    try {
     
     
      const userId = req.user._id
      
      let filename = null;
        
      const user = await userModel.findById(userId)

      
      
      
      if (req?.file  && user) {
        
        if (user?.avatar) {
          
          deleteImage(
            path.join(
              process.cwd(),
              "public",
              user.avatar.slice(32)
            )
          );


          filename = req?.file?.filename;
          const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;

          user.avatar = imagePath
        } else {
          filename = req?.file?.filename;
          const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;
          user.avatar = imagePath
        }

      }

      user.save()


      res.status(200).json({
        status:200,
        user,
        msg:'image uploaded'
      })

    } catch (err) {
      return next(new ErrorHandler(err.message, 400))
    }
  },

  
}