import { NextFunction, Request, Response } from "express";
import { catchAsynError } from "../middlewares/catchAsyncErrors";
import { reviewValidation } from "../validation/validation";
import { ErrorHandler } from "../utils/ErrorHandler";
import reviewsModel from "../models/reviews.model";
import { deleteImage } from "../utils/filesystem";
import path from "path";

export default {
  createReview : catchAsynError( async ( req: Request, res:Response, next:NextFunction) => {
    try {
      
      let filename = null;

      if (req.file) {
        filename = req.file.filename;
      }

      const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;



      const { owner_ru, owner_uz} = req.body

      const checkOwner_ru_name = await reviewsModel.findOne({owner_ru})
      const checkOwner_uz_name = await reviewsModel.findOne({owner_ru})


      if (checkOwner_ru_name) {
        return next(new ErrorHandler(`${owner_ru} user like this name already created`, 400))
      }

      
      if (checkOwner_uz_name) {
        return next(new ErrorHandler(`${owner_uz} user like this name already created`, 400))
      }


      const newReview = await reviewsModel.create({ ...req.body,avatar:filename ? imagePath : '' })

      res.status(201).json({
        status:201,
        review:'newReview',
        msg:'ok'
      })



    } catch (err) {
      return next(new ErrorHandler(err.message, 500))
    }
  }),

  deleteReview: catchAsynError( async ( req: Request, res:Response, next:NextFunction) => {
    try {
      
      const { id } = req.params

        

      if (!id) {
        return next(new ErrorHandler('id required', 400))
      }

      const checkReviw = await reviewsModel.findById(id)

      if (!checkReviw) {
        return next(new ErrorHandler('review not found', 400))
      }

       await checkReviw.deleteOne({id})
       deleteImage(
        path.join(
          process.cwd(),
          "public",
          "reviews",
          checkReviw.avatar.slice(32)
        ))
      

      res.status(200).json({
        status:200,
        review:checkReviw,
        msg:'ok'
      })

    } catch (err) {
      return next(new ErrorHandler(err.message, 500))
    }
  }),

  updateReview: catchAsynError(async (req:Request, res:Response, next:NextFunction) => {

    try {
      const { id } = req.params

      let filename = null;
      
      if (!id) {
        return next(new ErrorHandler('id required', 400))
      }

      const checkReviw = await reviewsModel.findById(id)

      if (!checkReviw) {
        return next(new ErrorHandler('review not found', 400))
      }

      if (req.file) {
        filename = req.file.filename
      }
      const imagePath = `${req.protocol}://${req.hostname}/api/media/${filename}`;


      const data = filename ? {...req.body, avatar:imagePath} : {...req.body}
      


      const updatedReview = await reviewsModel.findByIdAndUpdate(
        id,
        { $set: data },
        { new: true }
      );

      res.status(200).json({
        status: 200,
        review: updatedReview,
        msg: "ok",
      });
    } catch (err) {
      return next(new ErrorHandler(err.message, 500))
    }
  }),

  getOneReview: catchAsynError(async (req:Request, res:Response, next:NextFunction) => {

    try {
      const { id } = req.params

      if (!id) {
        return next(new ErrorHandler('id required', 400))
      }

      const checkReviw = await reviewsModel.findById(id)

      if (!checkReviw) {
        return next(new ErrorHandler('review not found', 400))
      }


      res.status(200).json({
        status: 200,
        review:checkReviw,
        msg: "ok",
      });
    } catch (err) {
      return next(new ErrorHandler(err.message, 500))
    }
  }),

  getAllReviews : catchAsynError(async (req:Request, res:Response, next:NextFunction) => {
    try {
      
      const reviews = await reviewsModel.find()

      res.status(200).json({
        status: 200,
        reviews,
        msg: "ok",
      });

    } catch (err) {
      return next(new ErrorHandler(err.message, 500))
    }
  })
}