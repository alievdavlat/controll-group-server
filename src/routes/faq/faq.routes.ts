import { Router } from "express";
import isAuthenticated from "../../middlewares/isAuthenticated";
import { authorizeRole } from "../../middlewares/isAdmin";
import faqController from "../../controllers/faq.controller";


const faqRouter = Router()

  faqRouter
    .post('/faq', isAuthenticated, authorizeRole('admin'), faqController.createFaq)    
    .get('/faqs', faqController.getAllFaq)
    .get('/faq/:id', faqController.getOneFaq)
    .put('/faq/:id', isAuthenticated, authorizeRole('admin'), faqController.updatedFaq)
    .delete('/faq/:id', isAuthenticated, authorizeRole('admin'), faqController.deleteFaq)
    
export default faqRouter