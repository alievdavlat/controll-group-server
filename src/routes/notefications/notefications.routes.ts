import { Router } from "express";
import contactController from "../../controllers/contact.controller";
import { get } from "http";
import isAuthenticated from "../../middlewares/isAuthenticated";
import { authorizeRole } from "../../middlewares/isAdmin";
import noteficationController from "../../controllers/notefication.controller";


const noteficationsRouter = Router()

  noteficationsRouter
      .get('/notefications', isAuthenticated, authorizeRole('admin'),noteficationController.getAllNotification)
      .put('/notefications/:id',isAuthenticated, authorizeRole('admin'), noteficationController.updateNotefication)

export default noteficationsRouter