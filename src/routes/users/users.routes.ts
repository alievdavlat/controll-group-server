import { Router } from "express";
import isAuthenticated from "../../middlewares/isAuthenticated";
import { authorizeRole } from "../../middlewares/isAdmin";
import userController from "../../controllers/user.controller";
import { upload } from "../../utils/multer";


const userRouter = Router()

  userRouter
    .put('/update-role', isAuthenticated, authorizeRole('user'), userController.changeUserRole) 
    .get('/get-profile', isAuthenticated, userController.getProfile)     
    .put('/update-info', isAuthenticated, userController.updateUserInfo)
    .put('/update-number', isAuthenticated, userController.updatePhoneNumber)
    .put('/update-avatar', isAuthenticated, upload.single('avatar'),  userController.updateProfilePicture)

export default userRouter