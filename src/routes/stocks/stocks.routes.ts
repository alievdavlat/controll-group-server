import { Router } from "express";
import isAuthenticated from "../../middlewares/isAuthenticated";
import { authorizeRole } from "../../middlewares/isAdmin";
import stocksController from "../../controllers/stocks.controller";
import { upload } from "../../utils/multer";


const stocksRouter = Router()

  stocksRouter
        .post('/stocks', isAuthenticated, authorizeRole('admin'), upload.single('image'), stocksController.createStocks)
        .get('/stocks', stocksController.getAllStocks)
        .get('/stocks/:id', stocksController.getOneStock)
        .put('/stocks/:id', isAuthenticated, authorizeRole('admin'), stocksController.updateStock)
        .delete('/stocks/:id', isAuthenticated, authorizeRole('admin'), stocksController.deleteStock)
        
export default stocksRouter