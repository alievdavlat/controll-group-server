import { Router } from "express";
import advantagesController from "../../controllers/advantages.controller";
import isAuthenticated from "../../middlewares/isAuthenticated";
import { authorizeRole } from "../../middlewares/isAdmin";
import { advantages_upload } from "../../utils/multer";

const advanatgesRouter = Router();

advanatgesRouter
  .post(
    "/create-advantages",
    isAuthenticated,
    authorizeRole("admin"),
    advantages_upload.single("img"),
    advantagesController.createAdvnatages
  )
  .put(
    "/update-advantages/:id",
    isAuthenticated,
    authorizeRole("admin"),
    advantages_upload.single("img"),
    advantagesController.updateAdvantages
  )
  .get(
    "/advantages/:id",
    advantagesController.getOneAdvantagesById
  )
  .get(
    "/advantages",
    advantagesController.getAllAdvantages
  )
  .delete(
    "/advantages/:id",
    isAuthenticated,
    authorizeRole("admin"),
    advantagesController.deletAdvantags
  );

export default advanatgesRouter;
