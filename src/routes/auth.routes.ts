import { Router } from "express";
import authController from "../controllers/auth.controller";


const authRouter = Router()

  authRouter
      .post('/signin', authController.login)
      .post('/signup', authController.register)
      

export default authRouter