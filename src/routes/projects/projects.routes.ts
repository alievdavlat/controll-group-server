import { Router } from "express";
import isAuthenticated from "../../middlewares/isAuthenticated";
import { authorizeRole } from "../../middlewares/isAdmin";
import { project_upload } from "../../utils/multer";
import projectsController from "../../controllers/projects.controller";

const projectsRouter = Router();

projectsRouter
  .post(
    "/projects",
    isAuthenticated,
    authorizeRole("admin"),
    project_upload.fields([
      { name: "img1" },
      { name: "img2" },
      { name: "img3" },
    ]),
    projectsController.createProject
  )
  .get("/projects", projectsController.getAllProjects)
  .get("/projects/:id", projectsController.getOneProject)
  .put(
    "/projects/:id",
    isAuthenticated,
    authorizeRole("admin"),
    projectsController.updateProject
  )
  .put(
    "/update-projects-image/:id",
    isAuthenticated,
    authorizeRole("admin"),
    project_upload.fields([
      { name: "img1" },
      { name: "img2" },
      { name: "img3" },
    ]),
    projectsController.updateProjectImages
  )
  .delete(
    "/projects/:id",
    isAuthenticated,
    authorizeRole("admin"),
    projectsController.deleteProject
  );

export default projectsRouter;
