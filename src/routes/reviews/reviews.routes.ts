import { Router } from "express";
import reviewsController from "../../controllers/reviews.controller";
import isAuthenticated from "../../middlewares/isAuthenticated";
import { authorizeRole } from "../../middlewares/isAdmin";
import { reviews_upload } from "../../utils/multer";


const reviewsRouter = Router()

  reviewsRouter
      .post('/create-review',reviews_upload.single('avatar') , reviewsController.createReview)
      .delete('/delete-review/:id', reviewsController.deleteReview)
      .put('/update-review/:id', isAuthenticated, authorizeRole('admin'), reviewsController.updateReview)
      .get('/reviews', reviewsController.getAllReviews)
      .get('/reviews/:id', reviewsController.getOneReview)


export default reviewsRouter