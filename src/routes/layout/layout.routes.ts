import { Router } from "express";
import { layout_upload } from "../../utils/multer";
import layoutController from "../../controllers/layout.controller";
import { authorizeRole } from "../../middlewares/isAdmin";
import isAuthenticated from "../../middlewares/isAuthenticated";

const layoutRouter = Router();

layoutRouter.post(
  "/layout/:type",
  isAuthenticated,
  authorizeRole("admin"),
  layout_upload.single("image"),
  layoutController.createLayout
);


layoutRouter.put(
  "/layout/:type",
  isAuthenticated,
  authorizeRole("admin"),
  layout_upload.single("image"),
  layoutController.editlayout
);


layoutRouter.get(
  '/layout/:type',
  layoutController.getLayoutByType
)


layoutRouter.delete(
  '/layout/:type',
  isAuthenticated, 
  authorizeRole('admin'),
  layoutController.deleteLayoutByType
)



export default layoutRouter;
