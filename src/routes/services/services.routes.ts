import { Router } from "express";
import isAuthenticated from "../../middlewares/isAuthenticated";
import { authorizeRole } from "../../middlewares/isAdmin";
import servicesController from "../../controllers/services.controller";
import { services_upload } from "../../utils/multer";

const servicesRouter = Router();

servicesRouter

  .post(
    "/create-service",
    isAuthenticated,
    authorizeRole("admin"),
    services_upload.single("img"),
    servicesController.createServices
  )
  .put(
    "/update-services-info/:id",
    isAuthenticated,
    authorizeRole("admin"),
    servicesController.updateServiceTexts
  )
  .put(
    "/update-service-img/:id",
    isAuthenticated,
    authorizeRole("admin"),
    services_upload.single("image"),
    servicesController.updateServicesImage
  )
  .delete(
    '/delete-service/:id',
    isAuthenticated,
    authorizeRole('admin'),
    servicesController.deleteService
  )
  .get(
    '/service/:id',
    servicesController.getOneService
  ) 
  .get(
    '/services',
    servicesController.getAllServices
  )


export default servicesRouter;
