import { Router } from "express";
import analyticsController from "../../controllers/analytics.controller";
import isAuthenticated from "../../middlewares/isAuthenticated";
import { authorizeRole } from "../../middlewares/isAdmin";


const analyticsRouter = Router()

  analyticsRouter
      .get('/analytics-contact',isAuthenticated,authorizeRole('admin'), analyticsController.getContactAnalytics )
      

export default analyticsRouter