import { Router } from "express";
import contactController from "../../controllers/contact.controller";
import { get } from "http";
import isAuthenticated from "../../middlewares/isAuthenticated";
import { authorizeRole } from "../../middlewares/isAdmin";


const contactRouter = Router()

  contactRouter
      .post('/contact', contactController.sendContact)
      .get('/contact', isAuthenticated, authorizeRole('admin'), contactController.getAllContact)
      .delete('/contact/:id',isAuthenticated, authorizeRole('admin'), contactController.deleteContact)

export default contactRouter