import { Router } from "express";
import layoutRouter from "./layout/layout.routes";
import advanatgesRouter from "./advantages/advantages.routes";
import contactRouter from "./contact/contact.routes";
import userRouter from "./users/users.routes";
import reviewsRouter from "./reviews/reviews.routes";
import servicesRouter from "./services/services.routes";
import noteficationsRouter from "./notefications/notefications.routes";
import analyticsRouter from "./analytics/analytics.routes";
import faqRouter from "./faq/faq.routes";
import stocksRouter from "./stocks/stocks.routes";
import projectsRouter from "./projects/projects.routes";


const MainRouter = Router()



export default MainRouter.use([
  layoutRouter,
  advanatgesRouter,
  contactRouter,
  userRouter,
  reviewsRouter,
  servicesRouter,
  noteficationsRouter,
  analyticsRouter,
  faqRouter,
  stocksRouter,
  projectsRouter
])