import { IUser } from "../models/user.model";
import { Request } from "express";



declare global {
  namespace Express {
    interface Request {
      user?:IUser
    }
  }
}


interface IUpdatePhoneNumber {
  newNumber:string;
  oldNumber:string;
}

interface IUpdateUserInfo {
  name:string
}


export {
  IUpdatePhoneNumber,
  IUpdateUserInfo
}



