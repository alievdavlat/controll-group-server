import { Request, Response, NextFunction } from "express";
import { ErrorHandler } from "../utils/ErrorHandler";
import jwt, { JwtPayload } from 'jsonwebtoken'
import { catchAsynError } from "./catchAsyncErrors";
import userModel from "../models/user.model";




export const isAuthenticated = catchAsynError(async (req:Request, res:Response, next:NextFunction) => {


  
  const token = req.headers.token;
  
  

  if (!token) {
    return next(new ErrorHandler('Please login to access to resource', 401))
  }
  const decoded = jwt.verify(token as any, process.env.SECRET_KEY as string) as JwtPayload


  if (!decoded) {
    return next(new ErrorHandler(' access token is not valid', 403))
  }

  const user = await userModel.findById(decoded.id) 

  

  if (!user) {
    return next(new ErrorHandler('Please login to access to resource', 404))
  }

  req.user = user
  
  next()

})

export default isAuthenticated