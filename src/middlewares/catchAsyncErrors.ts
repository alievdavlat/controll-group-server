import { NextFunction, Request, Response } from "express"

export  const catchAsynError = (theFunc:any) => (req:Request, res:Response, next:NextFunction ) =>  {
  Promise.resolve(theFunc(req, res, next )).catch(next)
}