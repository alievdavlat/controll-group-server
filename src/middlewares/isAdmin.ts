
import { NextFunction, Request, Response } from 'express'
import { ErrorHandler } from '../utils/ErrorHandler'




//validate user roles 


const authorizeRole = (...roles:string[]) => {
  return (req:Request, res:Response, next:NextFunction ) => {

    

    if(!roles.includes(req.user.role || '')){
      return next( new ErrorHandler(`Role ${req.user.role} is not allowed to access this resource`, 403))
    }

    next()
  }
}


export {
  authorizeRole
} 